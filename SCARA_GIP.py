"""GIP estimator of the inverse dynamics of the SCARA robot
Author: Alberto Dalla Libera
"""
import Project_Utils
import torch
import torch.utils.data
import gpr_lib.Utils.Parameters_covariance_functions as cov_functions
import gpr_lib.Likelihood.Gaussian_likelihood as Likelihood
import Models
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import argparse
import pickle as pkl

# Set arguments
p = argparse.ArgumentParser('SCARA robot GIP estimator')
p.add_argument('-data_path',
               type=str,
               default='SCARA_data/data_with_kin_error_omega2/rand_U',
               help='Data path')
p.add_argument('-model_path',
               type=str,
               default='Results/SCARA/Kin_error_omega2/',
               help='models path')
p.add_argument('-additional_path',
               type=str,
               default='_with_noise_run0',
               help='additional_path')
p.add_argument('-flg_load',
               type=bool,
               default=False,
               help='flg load model')
p.add_argument('-flg_save',
               type=bool,
               default=False,
               help='flg save model')
p.add_argument('-flg_train',
               type=bool,
               default=False,
               help='flg train ')
p.add_argument('-std_noise_kin_lin',
               type=int,
               default=4,
               help='noise in distances')
p.add_argument('-std_noise_kin_ang',
               type=int,
               default=4,
               help='noise in angles')
p.add_argument('-max_omega',
               type=float,
               default=3.,
               help='max omega')
p.add_argument('-batch_size',
               type=int,
               default=1000,
               help='batch_size')
p.add_argument('-shuffle',
               type=bool,
               default=False,
               help='shuffle')
p.add_argument('-flg_norm',
               type=bool,
               default=False,
               help='Normalize signal')
p.add_argument('-N_epoch',
               type=int,
               default=51,
               help='num epoch')
p.add_argument('-flg_cuda',
               type=bool,
               default=False,
               help='set the device type')
p.add_argument('-num_dat_tr',
               type=int,
               default=None,
               help='number of data to use in the training')
locals().update(vars(p.parse_known_args()[0]))


#### SET FILE PARAMETERS
print('---Set the file parameters')
# loading path
common_path = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path
tr_path = data_path+common_path+'_data_tr.pkl'
test_path = data_path+common_path+'_data_test.pkl'
print('tr_path',tr_path)
print('test_path',test_path)
# saving and loading path
model_saving_path = model_path+'Models/GIP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
model_loading_path = model_path+'Models/GIP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
estimate_tr_saving_path = model_path+'Estimates/GIP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_tr_hat.pt'
estimate_test_saving_path = model_path+'Estimates/GIP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'


#### SET OPTIMIZATION PARAMETERS
print('---Set the optimization')
# data loader parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.01, weight_decay=0)
N_epoch_print = 100
# set type
dtype=torch.float64


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
# set the joint to be considered
num_dof = 4
joint_index_list = range(0,num_dof)
# link_index_list = [0]
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0,0,1,0]
output_feature = 'tau'


#### LOAD THE DATASET
print('---Load data and get the dataset')
#training
input_tr, output_tr, active_dims_dict = Project_Utils.get_dataset_poly_from_structure(tr_path, num_dof, output_feature, robot_structure, features_name_list=joint_names)
#test
input_test, output_test, active_dims_dict = Project_Utils.get_dataset_poly_from_structure(test_path, num_dof, output_feature, robot_structure, features_name_list=joint_names)
# normaliziation
if flg_norm:
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# select the subset of training data to use
input_tr = input_tr[:num_dat_tr]
output_tr = output_tr[:num_dat_tr]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET THE MODEL
print('---Get the model')
# acc/vel kernel
acc_vel_gp_dict = dict()
acc_vel_gp_dict['name']='acc_vel'
acc_vel_gp_dict['active_dims'] = [active_dims_dict['active_dims_acc_vel']]
acc_vel_gp_dict['poly_deg'] = [1]
acc_vel_gp_dict['sigma_n_init'] = [0.1*np.ones(1)]
acc_vel_gp_dict['flg_train_sigma_n'] = [True]
acc_vel_gp_dict['Sigma_pos_par_init'] = [1*np.ones(1)] 
acc_vel_gp_dict['Sigma_free_par_init'] = [1*np.ones(active_dims_dict['active_dims_acc_vel'].size)]
acc_vel_gp_dict['flg_train_Sigma_pos_par'] = [True]
acc_vel_gp_dict['flg_train_Sigma_free_par'] = [True]
acc_vel_gp_dict['flg_offset'] = [True]
# position_kernel
deg_position = 2
position_gp_dict = dict()
position_gp_dict['name']='position_kernel'
position_gp_dict['active_dims'] = active_dims_dict['active_dims_mon_rev']+active_dims_dict['active_dims_mon_prism']
# position_gp_dict['active_dims'] = active_dims_dict['active_dims_mon_rev_cos']+active_dims_dict['active_dims_mon_rev_sin']+active_dims_dict['active_dims_mon_prism']
position_gp_dict['poly_deg'] = [deg_position]*len(position_gp_dict['active_dims'])
position_gp_dict['sigma_n_init'] = [None]*len(position_gp_dict['active_dims'])
position_gp_dict['flg_train_sigma_n'] = [False]*len(position_gp_dict['active_dims'])
Sigma_pos_par_position_all = [1*np.ones(1) for deg in range(deg_position)]*len(position_gp_dict['active_dims']) 
Sigma_pos_par_position = [np.concatenate(Sigma_pos_par_position_all[active_dim_index*deg_position:(active_dim_index+1)*deg_position])
                                     for active_dim_index in range(len(position_gp_dict['active_dims']))]
position_gp_dict['Sigma_pos_par_init'] = Sigma_pos_par_position
# Sigma_free_par_position_all =[0.2*np.ones(len(active_dim))/(deg+1)
#                          for active_dim in position_gp_dict['active_dims']
#                          for deg in range(0,deg_position)]
Sigma_free_par_position_all =[1*np.ones(len(active_dim))
                         for active_dim in position_gp_dict['active_dims']
                         for deg in range(0,deg_position)]
Sigma_free_par_position = [np.concatenate(Sigma_free_par_position_all[active_dim_index*deg_position:(active_dim_index+1)*deg_position])
                      for active_dim_index in range(len(position_gp_dict['active_dims']))]
position_gp_dict['Sigma_free_par_init'] = Sigma_free_par_position
position_gp_dict['flg_train_Sigma_pos_par'] = [True]*len(position_gp_dict['active_dims'])
position_gp_dict['flg_train_Sigma_free_par'] = [True]*len(position_gp_dict['active_dims'])
position_gp_dict['flg_offset'] = [True]*len(position_gp_dict['active_dims'])
gp_dict_list = [[acc_vel_gp_dict, position_gp_dict]]*num_dof
# downsampling mode
downsampling_mode='Downsampling'
# set the device
if flg_cuda:
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')
# initialize the model
m = Models.m_ind_GIP(num_dof, gp_dict_list,
                     name='SCARA_GIP', dtype=dtype, max_input_loc=10000, downsampling_mode=downsampling_mode,
                     sigma_n_num=None, device=device)
# move the model to the device
m.to(device)
m.print_model()

#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(1)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(model_loading_path))
# train the joint minimizing the negative MLL
if flg_train:
    print('Train the model with minimizing the negative MLL...')
    m.train_model(joint_index_list=joint_index_list,
                  X=input_tr, Y=output_tr,
                  criterion = Likelihood.Marginal_log_likelihood(), f_optimizer=f_optimizer,
                  batch_size=batch_size, shuffle = shuffle,
                  N_epoch=N_epoch, N_epoch_print=N_epoch_print)
# save the model
print('Save the model...')
if flg_save:
    torch.save(m.state_dict(), model_saving_path)


#### GET THE ESTIMATE
print('---Get estimate')
with torch.no_grad():
    print('Training estimate...')
    t_start = time.time()
    Y_tr_hat_list, var_tr_list, alpha_tr_list, m_X_list, K_X_inv_list, indices_list = m.get_torque_estimates(input_tr, output_tr, input_tr,
                                                                                                             joint_indices_list=joint_index_list,
                                                                                                             flg_return_K_X_inv=True)
    t_stop = time.time()
    print('Time elapsed ', t_stop-t_start)
    print('Test estimate...')
    t_start = time.time()
    Y_test_hat_list, var_test_list, _, _, _, _ = m.get_torque_estimates(input_tr, output_tr, input_test,
                                                                        alpha_list_par=alpha_tr_list, m_X_list_par=m_X_list,
                                                                        indices_list=indices_list,
                                                                        joint_indices_list=joint_index_list,
                                                                        K_X_inv_list_par=K_X_inv_list)
    t_stop = time.time()
    print('Time elapsed ', t_stop-t_start)


#### PRINT ESTIMATE AND STATS
Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                               output_test=output_test,
                                                                               Y_tr_hat_list=Y_tr_hat_list,
                                                                               Y_test_hat_list=Y_test_hat_list,
                                                                               flg_norm=flg_norm,
                                                                               norm_coeff=norm_coeff,
                                                                               joint_index_list=joint_index_list,
                                                                               output_feature=output_feature,
                                                                               var_tr_list=var_tr_list,
                                                                               var_test_list=var_test_list)
# save estimates
if flg_save:
    pkl.dump(Y_tr_hat_pd, open(estimate_tr_saving_path, 'wb'))
    pkl.dump(Y_test_hat_pd, open(estimate_test_saving_path, 'wb'))
# # print the estimates
# Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# plt.show()
#get the erros stats
Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)