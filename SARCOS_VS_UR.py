"""Comparison of the RBF estimator performance in the SARCOS dataset ant the UR10 dataset
Author: Alberto Dalla Libera"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib


# SARCOS results:
SARCOS_tr = [0.02681583461072450,
             0.01950015086251283,
             0.01303107578927546,
             0.00409664812674492,
             0.02602818247219018,
             0.02446443775022913,
             0.00814561391554278]

SARCOS_test = [0.0261272229535146,
               0.0189935816837355,
               0.0124099199160298,
               0.0043428950248647,
               0.0270640495324982,
               0.0228949589998979,
               0.0085422647131019]

UR10_tr = [0.1178305709989778,
           0.0305650543891784,
           0.0269290818139590,
           0.2159586460667561,
           0.2023432598484754,
           0.5436663169215997]

UR10_test = [0.2018634894046275,
             0.0408380114537700,
             0.0346788595170318,
             0.3782932865556451,
             0.3256237919070193,
             1.0456730342307252]


# Set position of bar on X axis
num_estimators = 2
num_joint_SARCOS = 7
num_joint_UR = 6
barWidth = 0.4

r1 = np.arange(1, num_joint_SARCOS+1)*3*barWidth
r2 = [x + barWidth for x in r1]

r3 = np.arange(1, num_joint_UR+1)*3*barWidth
r4 = [x + barWidth for x in r3]

# yticks
y_min = 0.
y_max = 1.1
yticks = np.arange(y_min, y_max, 0.1)
y_ticks_names = ['' for y in yticks]
for k in range(0,len(yticks),2):
    y_ticks_names[k] = "%.1f" % yticks[k]
# colors
colors = ['#006600', '#cccc00']
# matplotlib parameters
font = {'family' : 'normal',
        'size'   : 9}
#        'weight' : 'bold',
matplotlib.rc('font', **font)

matplotlib.rcParams['pdf.fonttype']=42
matplotlib.rcParams['ps.fonttype']=42

f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
plt.subplot(2,1,1)
plt.title('SARCOS')
plt.yscale('log')
plt.bar(r1, SARCOS_tr, color=colors[0], width=barWidth-0.05, edgecolor='black', label='TR')
plt.bar(r2, SARCOS_test, color=colors[1], width=barWidth-0.05, edgecolor='black', label='TEST')
# Add xticks on the middle of the group bars
plt.xlabel('Joint index', fontsize=12)
plt.ylabel('nMSE', fontsize=12)
plt.xticks([r + (num_estimators-1)/2*barWidth for r in r1], ['1', '2', '3', '4', '5', '6', '7'])
plt.ylim(bottom=y_min, top=y_max)
# Create legend & Show graphic
plt.legend(handlelength=1.5,handletextpad=.1, columnspacing=0.2, fontsize=12)      
plt.grid()
plt.subplot(2,1,2)
plt.title('UR10')
plt.yscale('log')
plt.bar(r3, UR10_tr, color=colors[0], width=barWidth-0.05, edgecolor='black', label='TR')
plt.bar(r4, UR10_test, color=colors[1], width=barWidth-0.05, edgecolor='black', label='TEST')
# Add xticks on the middle of the group bars
plt.xlabel('Joint index', fontsize=12)
plt.ylabel('nMSE', fontsize=12)
plt.xticks([r + (num_estimators-1)/2*barWidth for r in r3], ['1', '2', '3', '4', '5', '6'])
plt.ylim(bottom=y_min, top=y_max)
# Create legend & Show graphic
plt.legend(handlelength=1.5,handletextpad=.1, columnspacing=0.2, fontsize=12)      
plt.grid()
plt.savefig('SARCOS_vs_UR.pdf', bbox_inches='tight')
plt.show()
