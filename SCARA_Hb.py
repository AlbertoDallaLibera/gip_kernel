"""Regression matrix of the SCARA robot computed with sympybotics"""

import math
from math import sin, cos
def Hb(q, dq, ddq):
#
    Hb_out = [0]*32
#
    x0 = ddq[0] + ddq[1]
    x1 = math.cos(q[1])
    x2 = math.sin(q[1])
    x3 = 0.375*x1**2 + 0.375*x2**2
    x4 = 0.425*ddq[0]
    x5 = 0.425*dq[0]
    x6 = x2*x5
    x7 = -x6
    x8 = ddq[0]*x3 + 0.375*ddq[1] + dq[1]*x7 + x1*x4
    x9 = dq[0] + dq[1]
    x10 = x6*x9 + x8
    x11 = -x9**2
    x12 = dq[0]*x3 + x1*x5
    x13 = 0.375*dq[1]
    x14 = -dq[0]*x13 + dq[1]*x12 + x2*x4
    x15 = -x14
    x16 = x12 + x13
    x17 = x16*x9
    x18 = x15 + x17
    x19 = ddq[3] + x0
    x20 = math.sin(q[3])
    x21 = math.cos(q[3])
    x22 = x16*x20 + x21*x6
    x23 = dq[3] + x9
    x24 = -dq[3]*x22 + x15*x20 + x21*x8 + x22*x23
    x25 = -x23**2
    x26 = x20*x25
    x27 = x19*x21 + x26
    x28 = x21*x25
    x29 = x16*x21 + x20*x7
    x30 = -dq[3]*x29 - x14*x21 - x20*x8 + x23*x29
    x31 = -x19
    x32 = x20*x31 + x28
#
    Hb_out[0] = ddq[0]
    Hb_out[1] = x0
    Hb_out[2] = 0.425*x0*x1 + x0*x3 + x10 + 0.425*x11*x2
    Hb_out[3] = -0.425*x0*x2 + 0.425*x1*x11 + x11*x3 + x18
    Hb_out[4] = 0.425*x1*x10 + x10*x3 + 0.425*x2*(x14 - x17)
    Hb_out[5] = x19
    Hb_out[6] = 0.425*x1*x27 + 0.425*x2*(-x19*x20 + x28) + x24 + x27*x3
    Hb_out[7] = 0.425*x1*x32 + 0.425*x2*(x21*x31 - x26) + x3*x32 + x30
    Hb_out[8] = 0
    Hb_out[9] = x0
    Hb_out[10] = 0.375*x0 + x10
    Hb_out[11] = 0.375*x11 + x18
    Hb_out[12] = 0.375*x10
    Hb_out[13] = x19
    Hb_out[14] = x24 + 0.375*x27
    Hb_out[15] = x30 + 0.375*x32
    Hb_out[16] = 0
    Hb_out[17] = 0
    Hb_out[18] = 0
    Hb_out[19] = 0
    Hb_out[20] = ddq[2] + 9.81
    Hb_out[21] = 0
    Hb_out[22] = 0
    Hb_out[23] = 0
    Hb_out[24] = 0
    Hb_out[25] = 0
    Hb_out[26] = 0
    Hb_out[27] = 0
    Hb_out[28] = 0
    Hb_out[29] = x19
    Hb_out[30] = x24
    Hb_out[31] = x30
#
    return Hb_out