"""Accuracy plot
Author: Alberto Dalla Libera
"""

import pandas as pd
import Project_Utils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys

#folders path
data_path ='SCARA_data/data_with_kin_error_omega2_2000/'
results_path = 'Results/SCARA/Kin_error_omega2_2000/Estimates/'

#file parameters
num_run = 20
std_noise_kin_lin = 5
std_noise_kin_ang = 5
max_omega = 2.
additional_path = '_run'
additional_path_hat = '_with_noise_run'
num_dat_tr = 2000

#get the SCARA stats
SCARA_stat_list_PP = []
SCARA_stat_list_FE = []
SCARA_stat_list_SP = []
SCARA_stat_list_RBF = []
SCARA_stat_list_GIP = []
SCARA_stat_list_NN_sigmoid = []
SCARA_stat_list_SN = []
run_list = range(0,num_run)
for run in run_list:
    # additionals names
    additional_name = additional_path+str(run)
    additional_name_hat = additional_path_hat+str(run)
    experiment_name = str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_name
    experiment_name_hat = str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_name_hat
    # target data
    current_data_path = data_path+'rand_U_'+experiment_name+'_data_test.pkl'
    data = pd.read_pickle(current_data_path)
    # estimates
    results_additional_name = experiment_name_hat+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'
    results_path_PP = results_path+'PP_'+results_additional_name
    results_path_FE = results_path+'FE_'+results_additional_name
    results_path_SP = results_path+'SP_'+results_additional_name
    results_path_RBF = results_path+'RBF_'+results_additional_name
    results_path_GIP = results_path+'GIP_'+results_additional_name
    results_path_NN_sigmoid = results_path+'NN_sigmoid_'+results_additional_name
    results_path_SN = results_path+'SN_'+results_additional_name
    data_est_PP = pd.read_pickle(results_path_PP)
    data_est_FE = pd.read_pickle(results_path_FE)
    data_est_SP = pd.read_pickle(results_path_SP)
    data_est_RBF = pd.read_pickle(results_path_RBF)
    data_est_GIP = pd.read_pickle(results_path_GIP)
    data_est_NN_sigmoid= pd.read_pickle(results_path_NN_sigmoid)
    data_est_SN = pd.read_pickle(results_path_SN)
    # get errors
    stat_run_PP = Project_Utils.get_stat_estimate(data, [data_est_PP], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_FE = Project_Utils.get_stat_estimate(data, [data_est_FE], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_SP = Project_Utils.get_stat_estimate(data, [data_est_SP], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_RBF = Project_Utils.get_stat_estimate(data, [data_est_RBF], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_GIP = Project_Utils.get_stat_estimate(data, [data_est_GIP], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_NN_sigmoid = Project_Utils.get_stat_estimate(data, [data_est_NN_sigmoid], range(0,4), stat_name='nMSE', output_feature='tau')
    stat_run_SN = Project_Utils.get_stat_estimate(data, [data_est_SN], range(0,4), stat_name='nMSE', output_feature='tau')
    SCARA_stat_list_PP.append(stat_run_PP)
    SCARA_stat_list_FE.append(stat_run_FE)
    SCARA_stat_list_SP.append(stat_run_SP)
    SCARA_stat_list_RBF.append(stat_run_RBF)
    SCARA_stat_list_GIP.append(stat_run_GIP)
    SCARA_stat_list_NN_sigmoid.append(stat_run_NN_sigmoid)
    SCARA_stat_list_SN.append(stat_run_SN)

SCARA_stat_PP = [np.array([results[joint]  
                           for results in SCARA_stat_list_PP]).squeeze() 
                 for joint in range(0,4)]
SCARA_stat_FE = [np.array([results[joint]  
                           for results in SCARA_stat_list_FE]).squeeze() 
                 for joint in range(0,4)]
SCARA_stat_SP = [np.array([results[joint]  
                           for results in SCARA_stat_list_SP]).squeeze() 
                 for joint in range(0,4)]
SCARA_stat_RBF = [np.array([results[joint]  
                            for results in SCARA_stat_list_RBF]).squeeze() 
                  for joint in range(0,4)]
SCARA_stat_GIP = [np.array([results[joint]  
                            for results in SCARA_stat_list_GIP]).squeeze() 
                  for joint in range(0,4)]
SCARA_stat_NN_sigmoid = [np.array([results[joint]  
                                   for results in SCARA_stat_list_NN_sigmoid]).squeeze() 
                  for joint in range(0,4)]
SCARA_stat_SN = [np.array([results[joint]  
                 for results in SCARA_stat_list_SN]).squeeze() 
                  for joint in range(0,4)]
print('SCARA_stat_PP: ',SCARA_stat_PP)
print('SCARA_stat_FE: ',SCARA_stat_FE)
print('SCARA_stat_SP: ',SCARA_stat_SP)
print('SCARA_stat_RBF: ',SCARA_stat_RBF)
print('SCARA_stat_GIP: ',SCARA_stat_GIP)
print('SCARA_stat_NN_sigmoid: ',SCARA_stat_NN_sigmoid)
print('SCARA_stat_SN: ',SCARA_stat_SN)

# matplotlib parameters
font = {'family' : 'normal',
        'size'   : 12}
matplotlib.rc('font', **font)
matplotlib.rcParams['pdf.fonttype']=42
matplotlib.rcParams['ps.fonttype']=42

# # bar parameters
# colors = ['#006600', '#cccc00', '#0066cc', '#b30000', '#666633', '#00cc66']
# box_widths = 0.4
# box_dist = 0.45
# box_pos = [2*box_dist, 3*box_dist, 5*box_dist, 6*box_dist, 7*box_dist, box_dist]
# # xticks
# x_ticks = [box_dist, 2*box_dist, 3*box_dist, 4*box_dist, 5*box_dist, 6*box_dist, 7*box_dist]
# x_names = [ '$FE$', '$PP$', '$SP$', '', '$GIP$', '$RBF$', '$NN$']
# x_lim_max = 7*box_dist+box_widths/3*2
# x_lim_min = box_widths/3
# # y_ticks
# y_min = 5*10**(-7)
# y_max = 200
# ticks_exponent = range(2,-7,-1)
# y_ticks = [10**k for k in ticks_exponent]
# y_ticks_names = ['' for i in y_ticks]
# for k in range(0,len(y_ticks), 2):
#     y_ticks_names[k] = '$10^{'+str(ticks_exponent[k])+'}$'
# # plot
# fig = plt.figure()
# ax1 = plt.subplot(2,2,1)
# plt.grid()
# plt.title('Joint 1', fontsize=12)
# bp1_1 = ax1.boxplot(SCARA_stat_PP[0], positions=[box_pos[0]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[0]))
# bp2_1 = ax1.boxplot(SCARA_stat_SP[0], positions=[box_pos[1]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[1]))
# bp3_1 = ax1.boxplot(SCARA_stat_GIP[0], positions=[box_pos[2]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[2]))
# bp4_1 = ax1.boxplot(SCARA_stat_RBF[0], positions=[box_pos[3]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[3]))
# bp4_1 = ax1.boxplot(SCARA_stat_NN_sigmoid[0], positions=[box_pos[4]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[4]))
# bp5_1 = ax1.boxplot(SCARA_stat_FE[0], positions=[box_pos[5]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[5]))
# plt.yscale('log')
# plt.xlim(x_lim_min,x_lim_max)
# ax1.tick_params(labelbottom=False)
# plt.xticks(x_ticks)
# plt.ylabel('$nMSE$', fontsize=12)
# plt.ylim(top=y_max, bottom=y_min)
# plt.yticks(y_ticks, y_ticks_names, fontsize=12)

# ax2 = plt.subplot(2,2,2)
# plt.grid()
# plt.title('Joint 2', fontsize=12)
# plt.ylim(top=y_max, bottom=y_min)
# bp1_2 = ax2.boxplot(SCARA_stat_PP[1], positions=[box_pos[0]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[0]))
# bp2_2 = ax2.boxplot(SCARA_stat_SP[1], positions=[box_pos[1]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[1]))
# bp3_2 = ax2.boxplot(SCARA_stat_GIP[1], positions=[box_pos[2]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[2]))
# bp4_2 = ax2.boxplot(SCARA_stat_RBF[1], positions=[box_pos[3]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[3]))
# bp4_2 = ax2.boxplot(SCARA_stat_NN_sigmoid[1], positions=[box_pos[4]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[4]))
# bp5_2 = ax2.boxplot(SCARA_stat_FE[1], positions=[box_pos[5]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[5]))
# plt.yscale('log')
# plt.xlim(x_lim_min,x_lim_max)
# plt.xticks(x_ticks)
# plt.yticks(y_ticks)
# ax2.tick_params(labelbottom=False)
# ax2.tick_params(labelleft=False) 

# ax3 = plt.subplot(2,2,3)
# plt.grid()
# plt.ylim(top=y_max, bottom=y_min)
# plt.title('Joint 3', fontsize=12)
# bp1_3 = ax3.boxplot(SCARA_stat_PP[2], positions=[box_pos[0]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[0]))
# bp2_3 = ax3.boxplot(SCARA_stat_SP[2], positions=[box_pos[1]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[1]))
# bp3_3 = ax3.boxplot(SCARA_stat_GIP[2], positions=[box_pos[2]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[2]))
# bp4_3 = ax3.boxplot(SCARA_stat_RBF[2], positions=[box_pos[3]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[3]))
# bp4_3 = ax3.boxplot(SCARA_stat_NN_sigmoid[2], positions=[box_pos[4]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[4]))
# bp5_3 = ax3.boxplot(SCARA_stat_FE[2], positions=[box_pos[5]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[5]))
# plt.yscale('log')
# plt.xlim(x_lim_min,x_lim_max)
# plt.xticks(x_ticks, x_names)
# plt.ylabel('$nMSE$', fontsize=12)
# plt.yticks(y_ticks, y_ticks_names, fontsize=12)

# ax4 = plt.subplot(2,2,4)
# plt.grid()
# plt.title('Joint 4', fontsize=12)
# plt.ylim(top=y_max, bottom=y_min)
# bp1_4 = ax4.boxplot(SCARA_stat_PP[3], positions=[box_pos[0]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[0]))
# bp2_4 = ax4.boxplot(SCARA_stat_SP[3], positions=[box_pos[1]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[1]))
# bp3_4 = ax4.boxplot(SCARA_stat_GIP[3], positions=[box_pos[2]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[2]))
# bp4_4 = ax4.boxplot(SCARA_stat_RBF[3], positions=[box_pos[3]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[3]))
# bp5_4 = ax4.boxplot(SCARA_stat_NN_sigmoid[3], positions=[box_pos[4]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[4]))
# bp5_4 = ax4.boxplot(SCARA_stat_FE[3], positions=[box_pos[5]], notch=False, widths=box_widths, 
#                  patch_artist=True, boxprops=dict(facecolor=colors[5]))
# plt.yscale('log')
# plt.xlim(x_lim_min,x_lim_max)
# plt.xticks(x_ticks, x_names)
# plt.yticks(y_ticks)
# ax4.tick_params(labelleft=False) 
# fig.subplots_adjust(wspace=0.05, hspace=0.25)
# plt.savefig('SCARA_kin_error_neww.pdf', bbox_inches='tight')





# bar parameters
colors = ['#006600', '#cccc00', '#0066cc', '#b30000', '#666633', '#00cc66', '#990000']
box_widths = 0.4
box_dist = 0.45
box_pos = [2*box_dist, 3*box_dist, 5*box_dist, 6*box_dist, 7*box_dist, box_dist, 8*box_dist]
# xticks
x_ticks = [box_dist, 2*box_dist, 3*box_dist, 4*box_dist, 5*box_dist, 6*box_dist, 7*box_dist, 8*box_dist]
x_names = [ '$FE$', '$PP$', '$SP$', '', '$GIP$', '$RBF$', '$NN$', '$SN$']
x_lim_max = 8*box_dist+box_widths/3*2
x_lim_min = box_widths/3
# y_ticks
y_min = 5*10**(-7)
y_max = 5000
ticks_exponent = range(2,-7,-1)
y_ticks = [10**k for k in ticks_exponent]
y_ticks_names = ['' for i in y_ticks]
for k in range(0,len(y_ticks), 2):
    y_ticks_names[k] = '$10^{'+str(ticks_exponent[k])+'}$'
# plot
fig = plt.figure()
ax1 = plt.subplot(2,2,1)
plt.grid()
plt.title('Joint 1', fontsize=12)
bp1_1 = ax1.boxplot(SCARA_stat_PP[0], positions=[box_pos[0]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[0]))
bp2_1 = ax1.boxplot(SCARA_stat_SP[0], positions=[box_pos[1]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[1]))
bp3_1 = ax1.boxplot(SCARA_stat_GIP[0], positions=[box_pos[2]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[2]))
bp4_1 = ax1.boxplot(SCARA_stat_RBF[0], positions=[box_pos[3]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[3]))
bp4_1 = ax1.boxplot(SCARA_stat_NN_sigmoid[0], positions=[box_pos[4]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[4]))
bp5_1 = ax1.boxplot(SCARA_stat_FE[0], positions=[box_pos[5]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[5]))
bp5_1 = ax1.boxplot(SCARA_stat_SN[0], positions=[box_pos[6]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[6]))
plt.yscale('log')
plt.xlim(x_lim_min,x_lim_max)
ax1.tick_params(labelbottom=False)
plt.xticks(x_ticks)
plt.ylabel('$nMSE$', fontsize=12)
plt.ylim(top=y_max, bottom=y_min)
plt.yticks(y_ticks, y_ticks_names, fontsize=12)
ax2 = plt.subplot(2,2,2)
plt.grid()
plt.title('Joint 2', fontsize=12)
plt.ylim(top=y_max, bottom=y_min)
bp1_2 = ax2.boxplot(SCARA_stat_PP[1], positions=[box_pos[0]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[0]))
bp2_2 = ax2.boxplot(SCARA_stat_SP[1], positions=[box_pos[1]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[1]))
bp3_2 = ax2.boxplot(SCARA_stat_GIP[1], positions=[box_pos[2]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[2]))
bp4_2 = ax2.boxplot(SCARA_stat_RBF[1], positions=[box_pos[3]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[3]))
bp4_2 = ax2.boxplot(SCARA_stat_NN_sigmoid[1], positions=[box_pos[4]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[4]))
bp5_2 = ax2.boxplot(SCARA_stat_FE[1], positions=[box_pos[5]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[5]))
bp5_2 = ax2.boxplot(SCARA_stat_SN[1], positions=[box_pos[6]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[6]))
plt.yscale('log')
plt.xlim(x_lim_min,x_lim_max)
plt.xticks(x_ticks)
plt.yticks(y_ticks)
ax2.tick_params(labelbottom=False)
ax2.tick_params(labelleft=False) 
ax3 = plt.subplot(2,2,3)
plt.grid()
plt.ylim(top=y_max, bottom=y_min)
plt.title('Joint 3', fontsize=12)
bp1_3 = ax3.boxplot(SCARA_stat_PP[2], positions=[box_pos[0]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[0]))
bp2_3 = ax3.boxplot(SCARA_stat_SP[2], positions=[box_pos[1]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[1]))
bp3_3 = ax3.boxplot(SCARA_stat_GIP[2], positions=[box_pos[2]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[2]))
bp4_3 = ax3.boxplot(SCARA_stat_RBF[2], positions=[box_pos[3]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[3]))
bp4_3 = ax3.boxplot(SCARA_stat_NN_sigmoid[2], positions=[box_pos[4]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[4]))
bp5_3 = ax3.boxplot(SCARA_stat_FE[2], positions=[box_pos[5]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[5]))
bp5_3 = ax3.boxplot(SCARA_stat_SN[2], positions=[box_pos[6]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[6]))
plt.yscale('log')
plt.xlim(x_lim_min,x_lim_max)
plt.xticks(x_ticks, x_names)
plt.ylabel('$nMSE$', fontsize=12)
plt.yticks(y_ticks, y_ticks_names, fontsize=12)
ax4 = plt.subplot(2,2,4)
plt.grid()
plt.title('Joint 4', fontsize=12)
plt.ylim(top=y_max, bottom=y_min)
bp1_4 = ax4.boxplot(SCARA_stat_PP[3], positions=[box_pos[0]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[0]))
bp2_4 = ax4.boxplot(SCARA_stat_SP[3], positions=[box_pos[1]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[1]))
bp3_4 = ax4.boxplot(SCARA_stat_GIP[3], positions=[box_pos[2]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[2]))
bp4_4 = ax4.boxplot(SCARA_stat_RBF[3], positions=[box_pos[3]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[3]))
bp5_4 = ax4.boxplot(SCARA_stat_NN_sigmoid[3], positions=[box_pos[4]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[4]))
bp5_4 = ax4.boxplot(SCARA_stat_FE[3], positions=[box_pos[5]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[5]))
bp5_4 = ax4.boxplot(SCARA_stat_SN[3], positions=[box_pos[6]], notch=False, widths=box_widths, 
                 patch_artist=True, boxprops=dict(facecolor=colors[6]))
plt.yscale('log')
plt.xlim(x_lim_min,x_lim_max)
plt.xticks(x_ticks, x_names)
plt.yticks(y_ticks)
ax4.tick_params(labelleft=False) 
fig.subplots_adjust(wspace=0.05, hspace=0.25)
# plt.savefig('SCARA_kin_error_new_rev.pdf', bbox_inches='tight')
plt.show()