"""UR10 estimates plot
Author: Alberto Dalla Libera
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
 

num_joint = 6
num_estimators = 5 

# results
bars_PP = [0.1210011619896955,
           0.0159408290693384,
           0.0171693263159570,
           0.1815900439489726,
           0.2919862870623088,
           0.2727678238444591,]

bars_SP = [0.1291919845614172,
           0.0198770854194115,
           0.0132486889405849,
           0.1785890783700036,
           0.2476667732662931,
           0.2775628728332173]

bars_GIP = [0.1343154484107327,
            0.0177112616249683,
            0.0201978846156106,
            0.2035713530354748,
            0.2626697889861743,
            0.2419344913982526]

bars_RBF = [0.2018634894046275,
            0.0408380114537700,
            0.0346788595170318,
            0.3782932865556451,
            0.3256237919070193,
            1.0456730342307252]

bars_NN = [0.220577198989514,
           0.153562729787124,
           0.105520852483401,
           0.396808744066083,
           0.461546233409750,
           1.322723892797071]
 
# Set position of bar on X axis
barWidth = 0.3
r1 = np.arange(0, num_joint*(num_estimators+2)*barWidth, (num_estimators+2)*barWidth)
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]
# yticks
y_min = 0.
y_max = 1.1
yticks = np.arange(y_min, y_max, 0.1)
y_ticks_names = ['' for y in yticks]
for k in range(0,len(yticks),2):
    y_ticks_names[k] = "%.1f" % yticks[k]
# colors
colors = ['#006600', '#cccc00', '#0066cc', '#b30000', '#666633']
# matplotlib parameters
font = {'family' : 'normal',
        'size'   : 12}
#        'weight' : 'bold',
matplotlib.rc('font', **font)

matplotlib.rcParams['pdf.fonttype']=42
matplotlib.rcParams['ps.fonttype']=42
# Make the plot
ax = plt.axes()  
plt.bar(r1, bars_PP, color=colors[0], width=barWidth-0.05, edgecolor='black', label='PP')
plt.bar(r2, bars_SP, color=colors[1], width=barWidth-0.05, edgecolor='black', label='SP')
plt.bar(r3, bars_GIP, color=colors[2], width=barWidth-0.05, edgecolor='black', label='GIP')
plt.bar(r4, bars_RBF, color=colors[3], width=barWidth-0.05, edgecolor='black', label='RBF')
plt.bar(r5, bars_NN, color=colors[4], width=barWidth-0.05, edgecolor='black', label='NN')
# Add xticks on the middle of the group bars
plt.xlabel('Joint index', fontsize=12)
plt.ylabel('nMSE', fontsize=12)
plt.xticks([r + (num_estimators-1)/2*barWidth for r in r1], ['1', '2', '3', '4', '5', '6'])
plt.ylim(bottom=y_min, top=y_max)
plt.yticks(yticks, y_ticks_names)
# Create legend & Show graphic
plt.legend(handlelength=1.5,handletextpad=.1, columnspacing=0.2, fontsize=12)      
ax.yaxis.grid()
plt.savefig('UR10_nMSE.pdf', bbox_inches='tight')
plt.show()