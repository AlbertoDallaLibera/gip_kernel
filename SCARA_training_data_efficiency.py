"""Experiment 2: data efficiency test
Author: Alberto Dalla Libera
"""
import os

num_run = 1
std_noise_kin_lin = 0
std_noise_kin_ang = 0
max_omega = 2.
num_dat_tr_list = range(200, 4200, 200)
additional_path = '_with_noise_run'

###### GIP and PP training settings
# model_name_list = ['PP', 'GIP', 'RBF']
# N_steps_list = [51, 2001, 9001]
# batch_size = 200 

##### NN_sigmoid training settings (set cuda flag to True)
# model_name_list = ['NN_sigmoid']
# N_steps_list = [20001]
# batch_size = 100 

data_path = 'SCARA_data/data_no_kin_noise/rand_U'
model_path = 'Results/SCARA/No_kin_error/'

for model_index, model_name in enumerate(model_name_list):
    N_steps = N_steps_list[model_index]
    for num_dat_tr in num_dat_tr_list:
        N_epoch = int(N_steps/(num_dat_tr/batch_size)+1)
        for run in range(0,num_run):
            additional_path_run = additional_path+str(run)
            str_command = 'python SCARA_'+model_name+'.py'+\
                          ' -std_noise_kin_lin '+str(std_noise_kin_lin)+\
                          ' -std_noise_kin_ang '+str(std_noise_kin_ang)+\
                          ' -max_omega '+str(max_omega)+\
                          ' -additional_path '+additional_path_run+\
                          ' -data_path '+data_path+\
                          ' -model_path '+model_path+\
                          ' -num_dat_tr '+str(num_dat_tr)+\
                          ' -batch_size '+str(batch_size)+\
                          ' -N_epoch ' +str(N_epoch)+\
                          ' -flg_train True'+\
                          ' -shuffle True'#+\
#                          ' -flg_save True'
            print('\n##########\nstr_command: '+ str_command+'\n##########')
            os.system(str_command)