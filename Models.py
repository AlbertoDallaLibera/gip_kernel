"""Superclass of robot inverse dynamics estimator
Author: Alberto Dalla Libera"""
import torch
import gpr_lib.GP_prior.GP_prior as GP_prior
import gpr_lib.GP_prior.Sparse_GP as SGP
import gpr_lib.GP_prior.Stationary_GP as Stat_GP
import numpy as np
import gpr_lib.Utils.Parameters_covariance_functions as cov



class m_independent_joint(torch.nn.Module):
    """Model that considers each joint standalone"""


    def __init__(self,
                 num_dof,
                 name='',
                 dtype=torch.float64,
                 device=None):
        # init torch module
        super(m_independent_joint, self).__init__()
        # save perameters
        self.num_dof = num_dof
        self.name = name
        self.dtype = dtype
        self.device = device
        self.models_list = []


    def to(self, dev):
        """Move model to device"""
        super(m_independent_joint, self).to(dev)
        self.device = dev
        for model in self.models_list:
            model.to(dev)


    def print_model(self):
        """Print the model"""
        for joint_index, model in enumerate(self.models_list):
            print('Models joint torque '+str(joint_index+1)+':')
            model.print_model()


    def init_joint_torque_model(self, joint_index):
        """Init the model of the joint_index torque"""
        raise NotImplementedError


    def train_model(self, joint_index_list,
                    X, Y, 
                    criterion, f_optimizer,
                    batch_size, shuffle,
                    N_epoch, N_epoch_print,
                    additional_par_dict={}):
        """Train the torque models"""
        for joint_index in joint_index_list:
            print('\nJoint '+str(joint_index+1)+' training:')
            self.train_joint_model(joint_index=joint_index, 
                                   X=X, Y=Y[:,joint_index].reshape([-1,1]),
                                   criterion=criterion, f_optimizer=f_optimizer,
                                   batch_size=batch_size, shuffle=shuffle,
                                   N_epoch=N_epoch, N_epoch_print=N_epoch_print,
                                   **additional_par_dict) 


    def train_joint_model(self, joint_index,
                          X, Y,
                          criterion, f_optimizer,
                          batch_size, shuffle,
                          N_epoch, N_epoch_print):
        """Train the joint_index model"""
        # get the dataloader
        dataset = torch.utils.data.TensorDataset(torch.tensor(X,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device),
                                                 torch.tensor(Y,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device))
        trainloader = torch.utils.data.DataLoader(dataset,
                                                  batch_size=batch_size,
                                                  shuffle=shuffle)
        # get the optimizer
        optimizer = f_optimizer(self.models_list[joint_index].parameters())
        # trian the model
        for epoch in range(N_epoch):
            running_loss = 0.0
            N_btc = 0
            # print('\nEPOCH:', epoch)
            for i, data in enumerate(trainloader, 0):
                # get the inputs
                inputs, labels = data
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                out = self.models_list[joint_index](inputs)
                loss = criterion(out, labels)
                loss.backward(retain_graph=True)
                optimizer.step()
                running_loss = running_loss + loss.item()
                N_btc = N_btc + 1
            if epoch%N_epoch_print==0:
                print('\nEPOCH:', epoch)
                print('Runnung loss:', running_loss/N_btc)


    def get_torque_estimates(self, X_test, joint_indices_list=None):
        """Returns the joint torques estimate"""
        
        # check the joint to test
        if joint_indices_list is None:
            joint_indices_list = range(0,self.num_dof)
        # initialize the outputs
        estimate_list = []
        # get the estimate for each joint torques
        for i, joint_index in enumerate(joint_indices_list):
            print('\nJoint '+str(joint_index+1)+' estimate:')
            estimate = self.get_joint_torque_estimate(joint_index=joint_index,
                                                      X_test=torch.tensor(X_test,
                                                                          dtype=self.dtype,
                                                                          device=self.device,
                                                                          requires_grad=False))
            estimate_list.append(estimate.detach().cpu().numpy())
        return estimate_list


    def get_joint_torque_estimate(self, joint_index, X_test):
        """Return the estimate of the joint_index torque""" 
        return self.models_list[joint_index](X_test)






##############################
#----------GP MODELS---------#
##############################




class m_indep_GP(m_independent_joint):
    """Superclass of the models based on GP"""
    def __init__(self, num_dof,
                 active_dims_list, 
                 sigma_n_init_list=None, flg_train_sigma_n=True,
                 name='', dtype=torch.float64, device=None,
                 max_input_loc=1000, downsampling_mode='Downsampling', sigma_n_num=None):
        super(m_indep_GP, self).__init__(num_dof=num_dof,
                                         name=name,
                                         dtype=dtype,
                                         device=device)
        self.active_dims_list = active_dims_list
        self.sigma_n_init_list = sigma_n_init_list
        self.flg_train_sigma_n = flg_train_sigma_n
        self.max_input_loc = max_input_loc
        self.downsampling_mode = downsampling_mode
        self.sigma_n_num = sigma_n_num


    def train_joint_model(self, joint_index, 
                          X, Y,
                          criterion, f_optimizer,
                          batch_size, shuffle,
                          N_epoch, N_epoch_print):
        # get the dataloader
        dataset = torch.utils.data.TensorDataset(torch.tensor(X,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device),
                                                 torch.tensor(Y,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device))
        trainloader = torch.utils.data.DataLoader(dataset,
                                                  batch_size=batch_size,
                                                  shuffle=shuffle)
        # fit the model
        self.models_list[joint_index].fit_model(trainloader=trainloader, 
                                                optimizer=f_optimizer(self.models_list[joint_index].parameters()), criterion=criterion,
                                                N_epoch=N_epoch, N_epoch_print=N_epoch_print,
                                                flg_time=False, f_saving_model=None, f_print=None)
        if self.device.type=='cuda':
            torch.cuda.empty_cache()


    def get_torque_estimates(self, X, Y, X_test,
                             alpha_list_par=None, m_X_list_par=None, K_X_inv_list_par=None,
                             flg_return_K_X_inv=False,
                             indices_list=None, joint_indices_list=None):
        """Performs the estimate for each joint torques"""
        if joint_indices_list is None:
            joint_indices_list = range(0,self.num_dof)
        # check alpha_list and K_X_inv_inv
        if alpha_list_par is None:
            alpha_list_par = [None for i in range(0,self.num_dof)]
        if K_X_inv_list_par is None:
            K_X_inv_list_par = [None for i in range(0,self.num_dof)]
        if m_X_list_par is None:
            m_X_list_par = [None for i in range(0,self.num_dof)]
        # check the number of data and if required downsample
        if indices_list is None:
            if X.shape[0]>self.max_input_loc:
                indices_list = self.downsample_data(X,Y)
            else:
                indices_list = [range(0,X.shape[0]) for joint_index in range(0,self.num_dof)]
        # initialize the outputs
        estimate_list = []
        var_list = []
        alpha_list = []
        m_X_list = []
        K_X_inv_list = []
        # get the estimate for each link
        for i, joint_index in enumerate(joint_indices_list):
            print('\nLink '+str(joint_index+1)+' estimate:')
            estimate, var, alpha, m_X, K_X_inv = self.get_joint_torque_estimate(joint_index=joint_index,
                                                                                X=torch.tensor(X[indices_list[joint_index],:],
                                                                                               dtype=self.dtype,
                                                                                               device=self.device,
                                                                                               requires_grad=False),
                                                                                Y=torch.tensor(Y[indices_list[joint_index],joint_index].reshape([-1,1]),
                                                                                               dtype=self.dtype,
                                                                                               device=self.device,
                                                                                               requires_grad=False),
                                                                                X_test=torch.tensor(X_test,
                                                                                                    dtype=self.dtype,
                                                                                                    device=self.device,
                                                                                                    requires_grad=False),
                                                                                flg_return_K_X_inv=flg_return_K_X_inv,
                                                                                alpha_par=alpha_list_par[i], 
                                                                                m_X_par=m_X_list_par[i],
                                                                                K_X_inv_par=K_X_inv_list_par[i])
            estimate_list.append(estimate.detach().cpu().numpy())
            if var is not None:
                var_list.append(var.detach().cpu().numpy().reshape([-1,1]))
            else:
                var_list.append(var)
            alpha_list.append(alpha)
            m_X_list.append(m_X)
            K_X_inv_list.append(K_X_inv)
        return estimate_list, var_list, alpha_list, m_X_list, K_X_inv_list, indices_list


    def get_joint_torque_estimate(self, joint_index,
                                  X, Y, X_test,
                                  flg_return_K_X_inv=False, alpha_par=None,
                                  m_X_par=None, K_X_inv_par=None):
        """Return the estimate of the joint_index torque""" 
        if alpha_par is None:
            if flg_return_K_X_inv:
                return self.models_list[joint_index].get_estimate(X=X,
                                                                  Y=Y,
                                                                  X_test=X_test,
                                                                  Y_test=None, 
                                                                  flg_return_K_X_inv=flg_return_K_X_inv)
            else:
                Y_hat, var, alpha = self.models_list[joint_index].get_estimate(X=X,
                                                                               Y=Y,
                                                                               X_test=X_test,
                                                                               Y_test=None,
                                                                               flg_return_K_X_inv=flg_return_K_X_inv)
        else:
            if K_X_inv_par is None:
                Y_hat = self.models_list[joint_index].get_estimate_from_alpha(X=X,
                                                                              X_test=X_test,
                                                                              alpha=alpha_par,
                                                                              m_X=m_X_par,
                                                                              K_X_inv=K_X_inv_par,
                                                                              Y_test=None)
                var = None
                alpha = None
            else:
                Y_hat, var = self.models_list[joint_index].get_estimate_from_alpha(X=X,
                                                                                   X_test=X_test,
                                                                                   alpha=alpha_par,
                                                                                   m_X=m_X_par,
                                                                                   K_X_inv=K_X_inv_par,
                                                                                   Y_test=None) 
                alpha = None
        K_X_inv = None
        m_X = None
        return Y_hat, var, alpha, m_X, K_X_inv


    def downsample_data(self, X, Y):
        """Downsample data"""
        if self.downsampling_mode == 'Downsampling':
            print('Downsampling....')
            num_sample = X.shape[0]
            downsampling_step = int(num_sample/self.max_input_loc)
            indices = range(0,num_sample, downsampling_step)
            indices_list = [indices for joint_index in range(0,self.num_dof)]
        elif self.downsampling_mode == 'Random':
            print('Random downsampling')
            num_sample = X.shape[0]
            indices = random.sample(range(0,num_sample), self.max_input_loc)
            indices_list = [indices for joint_index in range(0,self.num_dof)]
        return indices_list





class m_ind_RBF(m_indep_GP):
    """Model that considers each link standalone
       and models each gp with a RBF kernel"""
    def __init__(self, num_dof, active_dims_list,
                 sigma_n_init_list=None, flg_train_sigma_n=True,
                 lengthscales_init_list=None, flg_train_lengthscales_list=True,
                 lambda_init_list=None, flg_train_lambda_list=True,
                 mean_init_list=None, flg_train_mean_list = False, 
                 name='RBF', dtype=torch.float64, device=None,
                 max_input_loc=1000, downsampling_mode='Downsampling', sigma_n_num=None):
        # initialize the superclass
        super(m_ind_RBF, self).__init__(num_dof=num_dof,
                                        active_dims_list=active_dims_list, 
                                        sigma_n_init_list=sigma_n_init_list, flg_train_sigma_n=flg_train_sigma_n,
                                        name=name, dtype=dtype, device=device,
                                        max_input_loc=max_input_loc, downsampling_mode=downsampling_mode, sigma_n_num=sigma_n_num)
        # save model parameters
        self.lengthscales_init_list = lengthscales_init_list
        self.lambda_init_list = lambda_init_list
        self.mean_init_list = mean_init_list
        self.flg_train_lengthscales_list = flg_train_lengthscales_list
        self.flg_train_lambda_list = flg_train_lambda_list
        self.flg_train_mean_list = flg_train_mean_list
        # initialize initial par if not initialized
        if lengthscales_init_list is None:
            self.lengthscales_init_list = [None for active_dims_link in active_dims_list]
        if lambda_init_list is None:
            self.lambda_init_list = [None for active_dims_link in active_dims_list]
        if mean_init_list is None:
            self.mean_init_list = [None for active_dims_link in active_dims_list]
        # get the GP of each link
        self.models_list = torch.nn.ModuleList([self.init_joint_torque_model(joint_index)
                                                for joint_index in range(0,num_dof)])


    def init_joint_torque_model(self, joinr_index):
        """Return a RBF gp"""
        return Stat_GP.RBF(self.active_dims_list[joinr_index],
                           sigma_n_init=self.sigma_n_init_list[joinr_index], flg_train_sigma_n=self.flg_train_sigma_n,
                           lengthscales_init=self.lengthscales_init_list[joinr_index], flg_train_lengthscales=self.flg_train_lengthscales_list[joinr_index],
                           lambda_init=self.lambda_init_list[joinr_index], flg_train_lambda=self.flg_train_lambda_list[joinr_index],
                           name=self.name+'RBF_'+str(joinr_index), dtype=self.dtype, sigma_n_num=self.sigma_n_num, device=self.device)




class m_ind_LIN(m_indep_GP):
    """Model that considers each link standalone
       and models each gp with a linear kernel"""
    def __init__(self, num_dof, active_dims_list,
                 sigma_n_init_list=None, flg_train_sigma_n=True,
                 Sigma_function_list=None, Sigma_f_additional_par_list=None, 
                 Sigma_pos_par_init_list=None, flg_train_Sigma_pos_par=True,
                 Sigma_free_par_init_list=None, flg_train_Sigma_free_par=True,
                 name='', dtype=torch.float64, max_input_loc=1000, downsampling_mode='Downsampling', sigma_n_num=None, device=None):
        super(m_ind_LIN, self).__init__(num_dof=num_dof,
                                        active_dims_list=active_dims_list, 
                                        sigma_n_init_list=sigma_n_init_list, flg_train_sigma_n=flg_train_sigma_n,
                                        name=name, dtype=dtype, device=device,
                                        max_input_loc=max_input_loc, downsampling_mode=downsampling_mode, sigma_n_num=sigma_n_num)
        # save parameters of the linear kernel
        self.Sigma_function_list = Sigma_function_list
        self.Sigma_f_additional_par_list = Sigma_f_additional_par_list
        self.flg_train_Sigma_pos_par = flg_train_Sigma_pos_par
        self.flg_train_Sigma_free_par = flg_train_Sigma_free_par
        self.Sigma_pos_par_init_list = Sigma_pos_par_init_list
        self.Sigma_free_par_init_list = Sigma_free_par_init_list
        # check parameters initialization
        if Sigma_pos_par_init_list is None:
            self.Sigma_pos_par_init_list = [None for active_dims_joint in active_dims_list]
        if Sigma_free_par_init_list is None:
            self.Sigma_free_par_init_list = [None for active_dims_joint in active_dims_list]

        self.models_list = torch.nn.ModuleList([self.init_joint_torque_model(joint_index)
                                                for joint_index in range(0,num_dof)])


    def init_joint_torque_model(self, joint_index):
        """Returns a linear gp"""
        return SGP.Linear_GP(self.active_dims_list[joint_index],
                             sigma_n_init=self.sigma_n_init_list[joint_index], flg_train_sigma_n=self.flg_train_sigma_n,
                             Sigma_function=self.Sigma_function_list[joint_index], Sigma_f_additional_par_list=self.Sigma_f_additional_par_list[joint_index], 
                             Sigma_pos_par_init=self.Sigma_pos_par_init_list[joint_index], flg_train_Sigma_pos_par=self.flg_train_Sigma_pos_par,
                             Sigma_free_par_init=self.Sigma_free_par_init_list[joint_index], flg_train_Sigma_free_par=self.flg_train_Sigma_free_par,
                             name=self.name+'PP_'+str(joint_index), dtype=self.dtype, device=self.device)


class m_ind_SP(m_ind_LIN):
    """Semi-parametric model (gp with kernel function given by linear kernel + RBF kernel)"""
    def __init__(self,num_dof, active_dims_list_PP, active_dims_list_RBF,
                 sigma_n_init_list=None, flg_train_sigma_n=True,
                 Sigma_function_list=None, Sigma_f_additional_par_list=None, 
                 Sigma_pos_par_init_list=None, flg_train_Sigma_pos_par=True,
                 Sigma_free_par_init_list=None, flg_train_Sigma_free_par=True,
                 lengthscales_init_list=None, flg_train_lengthscales_list=True,
                 lambda_init_list=None, flg_train_lambda_list=True,
                 name='SP', dtype=torch.float64, max_input_loc=1000, downsampling_mode='Downsampling',
                 sigma_n_num=None, device=None):
        # assign the RBF parameters
        if lengthscales_init_list is None:
            active_dims_list_RBF = [None for active_dims_link in active_dims_list_RBF]
        if lambda_init_list is None:
            lambda_init_list = [None for active_dims_link in active_dims_list_RBF]
        self.active_dims_list_RBF = active_dims_list_RBF
        self.lengthscales_init_list = lengthscales_init_list
        self.flg_train_lengthscales_list = flg_train_lengthscales_list
        self.lambda_init_list = lambda_init_list
        self.flg_train_lambda_list = flg_train_lambda_list
        # initialize the model
        super(m_ind_SP, self).__init__(num_dof=num_dof, active_dims_list=active_dims_list_PP,
                                       sigma_n_init_list=sigma_n_init_list, flg_train_sigma_n=flg_train_sigma_n,
                                       Sigma_function_list=Sigma_function_list, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                                       Sigma_pos_par_init_list=Sigma_pos_par_init_list, flg_train_Sigma_pos_par=flg_train_Sigma_pos_par,
                                       Sigma_free_par_init_list=Sigma_free_par_init_list, flg_train_Sigma_free_par=flg_train_Sigma_free_par,
                                       name=name, dtype=dtype, max_input_loc=max_input_loc, downsampling_mode=downsampling_mode,
                                       sigma_n_num=sigma_n_num, device=device)

    
    def init_joint_torque_model(self, joint_index):
        """Returns a gp with kernel given by the sum of a linear kernel and a RBF kernel"""
        GP_list_sum = []
        #get the PP
        GP_list_sum.append(super(m_ind_SP, self).init_joint_torque_model(joint_index))
        #get the RBF
        GP_list_sum.append(Stat_GP.RBF(self.active_dims_list_RBF[joint_index],
                                       sigma_n_init=None, flg_train_sigma_n=False,
                                       lengthscales_init=self.lengthscales_init_list[joint_index], flg_train_lengthscales=self.flg_train_lengthscales_list[joint_index],
                                       lambda_init=self.lambda_init_list[joint_index], flg_train_lambda=self.flg_train_lambda_list[joint_index],
                                       name=self.name+'_RBF_'+str(joint_index), dtype=self.dtype, sigma_n_num=self.sigma_n_num, device=self.device))
        #return the sum of the two kernels
        return GP_prior.Sum_Independent_GP(*GP_list_sum)




class m_ind_GIP(m_indep_GP):
    """Implementation of the robot inverse dynamics estimator based on GIP kernel"""

    def __init__(self, num_dof, gp_dict_list,
                 name='', dtype=torch.float64,
                 max_input_loc=1000, downsampling_mode='Downsampling',
                 sigma_n_num=None, device=None):

        # initialize the superclass
        super(m_ind_GIP, self).__init__(num_dof=num_dof,
                                        active_dims_list=None, 
                                        sigma_n_init_list=None, flg_train_sigma_n=None,
                                        name=name, dtype=dtype,
                                        max_input_loc=max_input_loc, downsampling_mode=downsampling_mode,
                                        sigma_n_num=sigma_n_num, device=device)
        # save model par dict
        self.gp_dict_list = gp_dict_list
        # get the GP of each link
        self.models_list = torch.nn.ModuleList([self.init_joint_torque_model(joint_index)
                                                for joint_index in range(0,num_dof)])


    def init_joint_torque_model(self, joint_index):
        """Returns a gp with kernel given by thge product of MPK"""
        GP_list = []
        for gp_dict in self.gp_dict_list[joint_index]:
            GP_list = GP_list + self.get_gp_list_from_dict(joint_index, gp_dict)
        GP = GP_prior.Multiply_GP_prior(*GP_list)
        return GP

    def get_gp_list_from_dict(self, joint_index, gp_dict):
        """Returns a list of MPK object"""
        GP_list = []
        num_gp = len(gp_dict['active_dims'])
        for gp_index in range(0,num_gp):
            GP_list.append(SGP.MPK_GP(active_dims=gp_dict['active_dims'][gp_index], poly_deg=gp_dict['poly_deg'][gp_index],
                                      sigma_n_init=gp_dict['sigma_n_init'][gp_index], flg_train_sigma_n=gp_dict['flg_train_sigma_n'][gp_index],
                                      Sigma_pos_par_init=gp_dict['Sigma_pos_par_init'][gp_index], flg_train_Sigma_pos_par=gp_dict['flg_train_Sigma_pos_par'][gp_index],
                                      Sigma_free_par_init=gp_dict['Sigma_free_par_init'][gp_index], flg_train_Sigma_free_par=gp_dict['flg_train_Sigma_free_par'][gp_index],
                                      flg_offset=gp_dict['flg_offset'][gp_index],
                                      name=self.name+'_'+str(joint_index+1)+'_'+gp_dict['name']+'_'+str(gp_index),
                                      dtype=self.dtype, sigma_n_num=self.sigma_n_num,device=self.device))
        return GP_list




class m_ind_GIP_with_friction(m_ind_GIP):
    """Implementation of the robot inverse dynamics estimator based on GIP kernel
    plus a linear kernel modeling frictions"""
    def __init__(self, num_dof, gp_dict_list, gp_friction_dict_list,
                 name='', dtype=torch.float64, max_input_loc=1000, downsampling_mode='Downsampling',
                 sigma_n_num=None, device=None, downsampling_threshold_list=[]):
        # save gp_dict of frictions object
        self.gp_friction_dict_list=gp_friction_dict_list
        super(m_ind_GIP_with_friction, self).__init__(num_dof=num_dof, gp_dict_list=gp_dict_list,
                                                      name=name, dtype=dtype, max_input_loc=max_input_loc,
                                                      downsampling_mode=downsampling_mode,
                                                      sigma_n_num=sigma_n_num, device=device)


    def init_joint_torque_model(self, joint_index):
        return GP_prior.Sum_Independent_GP(super(m_ind_GIP_with_friction, self).init_joint_torque_model(joint_index),
                                           SGP.Linear_GP(active_dims=self.gp_friction_dict_list[joint_index]['active_dims'],
                                                         mean_init=None, flg_mean_trainable=False, flg_no_mean=True,
                                                         sigma_n_init=None, flg_train_sigma_n=False,
                                                         Sigma_function=self.gp_friction_dict_list[joint_index]['Sigma_function'],
                                                         Sigma_f_additional_par_list=self.gp_friction_dict_list[joint_index]['Sigma_f_additional_par_list'], 
                                                         Sigma_pos_par_init=self.gp_friction_dict_list[joint_index]['Sigma_pos_par_init'],
                                                         flg_train_Sigma_pos_par=self.gp_friction_dict_list[joint_index]['flg_train_Sigma_pos_par'],
                                                         Sigma_free_par_init=self.gp_friction_dict_list[joint_index]['Sigma_free_par_init'],
                                                         flg_train_Sigma_free_par=self.gp_friction_dict_list[joint_index]['flg_train_Sigma_free_par'],
                                                         flg_offset=False,
                                                         name=self.name+'_'+str(joint_index+1)+'_'+self.gp_friction_dict_list[joint_index]['name'],
                                                         dtype=self.dtype, sigma_n_num=self.sigma_n_num,device=self.device))




##############################
#----------NN MODELS---------#
##############################



class m_indep_NN_sigmoid(m_independent_joint):
    """Class that mdoels each joint torque with a two layer NN with
    sigmoids as activation functions"""
    def __init__(self,
                 num_dof,
                 num_input,
                 num_unit_1_list,
                 num_unit_2_list,
                 name='',
                 dtype=torch.float64,
                 device=None):
        super(m_indep_NN_sigmoid, self).__init__(num_dof=num_dof,
                                                 name=name,
                                                 dtype=dtype,
                                                 device=device)
        # save the model parameters
        self.num_input = num_input
        self.num_unit_1_list = num_unit_1_list
        self.num_unit_2_list = num_unit_2_list
        # init the model of each joint torque
        torch.set_default_dtype(dtype)
        self.models_list = torch.nn.ModuleList([self.init_joint_torque_model(joint_index)
                                                for joint_index in range(0,num_dof)])


    def init_joint_torque_model(self, joint_index):
        """Init the model of the joint_index torque"""
        return Deep_NN_sigmoid(self.num_input,
                               self.num_unit_1_list[joint_index],
                               self.num_unit_2_list[joint_index])




class m_indep_NN_relu(m_indep_NN_sigmoid):
    """Class that mdoels each joint torque with a two layer NN with
    relu as activation functions"""
        
    def init_joint_torque_model(self, joint_index):
        """Init the model of the joint_index torque"""
        return Deep_NN_relu(self.num_input,
                            self.num_unit_1_list[joint_index],
                            self.num_unit_2_list[joint_index]) 




class m_indep_NN(m_indep_NN_sigmoid):
    """Class that mdoels each joint torque with a two layer NN.
    Activation functions of the first layer are sigmoid, while
    the activation functions of the second layer are relu"""
        
    def init_joint_torque_model(self, joint_index):
        """Init the model of the joint_index torque"""
        return Deep_NN(self.num_input,
                       self.num_unit_1_list[joint_index],
                       self.num_unit_2_list[joint_index])   




class Deep_NN_sigmoid(torch.nn.Module):
    """Two layer fully connected NN with sigmoid as activation functions"""
    
    def __init__(self, num_input, num_unit_1, num_unit_2):
        super(Deep_NN_sigmoid, self).__init__()
        self.f1 = torch.nn.Linear(num_input, num_unit_1, bias=True)
        self.f2 = torch.nn.Linear(num_unit_1, num_unit_2, bias=True)
        self.f3 = torch.nn.Linear(num_unit_2, 1, bias=True)

    def forward(self, x):
        layer_1 = torch.sigmoid(self.f1(x))
        layer_2 = torch.sigmoid(self.f2(layer_1)) 
        return self.f3(layer_2)




class Deep_NN_relu(torch.nn.Module):
    """Two layer fully connected NN with relu as activation functions"""
    
    def __init__(self, num_input, num_unit_1, num_unit_2):
        super(Deep_NN_relu, self).__init__()
        self.f1 = torch.nn.Linear(num_input, num_unit_1, bias=True)
        self.f2 = torch.nn.Linear(num_unit_1, num_unit_2, bias=True)
        self.f3 = torch.nn.Linear(num_unit_2, 1, bias=True)

    def forward(self, x):
        layer_1 = torch.relu(self.f1(x))
        layer_2 = torch.relu(self.f2(layer_1)) 
        return self.f3(layer_2)




class Deep_NN(torch.nn.Module):
    """Two layer fully connected NN.
    The activation functions of the first layer are sigmoids,
    while the second layer is composed of relu functions"""
    
    def __init__(self, num_input, num_unit_1, num_unit_2):
        super(Deep_NN, self).__init__()
        self.f1 = torch.nn.Linear(num_input, num_unit_1, bias=True)
        self.f2 = torch.nn.Linear(num_unit_1, num_unit_2, bias=True)
        self.f3 = torch.nn.Linear(num_unit_2, 1, bias=True)

    def forward(self, x):
        layer_1 = torch.sigmoid(self.f1(x))
        layer_2 = F.relu(self.f2(layer_1)) 
        return self.f3(layer_2)     