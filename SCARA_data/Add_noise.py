"""Analize data and add noise
Author: Alberto Dalla Libera"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle as pkl

# data par
# data_path = 'data_with_kin_error_omega2/rand_U'
# data_path = 'data_with_kin_error_omega1/rand_U'
data_path = 'data_no_kin_noise/rand_U'
max_omega = 2.
std_noise_kin_lin = 0
std_noise_kin_ang = 0
num_run = 20
additional_path = '_run'
additional_path_noisy = '_with_noise_run'
flg_add_noise = True
# std_noise = np.sqrt(np.array([419.58669755, 102.12423519, 1.51999686, 0.51872588]))/100
# std_noise = np.sqrt(np.array([6161.69102017, 1565.74458706, 33.67209763, 8.6031503]))/100
std_noise = 0.01
ouput_labels = ['tau_'+str(joint_index)
                 for joint_index in range(1,5)]
# initialize stats
max_vel = np.zeros(4)
max_acc = np.zeros(4)
mean_var_output = np.zeros(4)
# iterate over the different run
for run in range(0,num_run):
    print('Adding noise to run '+str(run))
    # loading path
    common_path = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path+str(run)
    tr_path = data_path+common_path+'_data_tr.pkl'
    test_path = data_path+common_path+'_data_test.pkl'
    # load data
    data_frame_tr = pd.read_pickle(tr_path)
    data_frame_test = pd.read_pickle(test_path)
    # get stats
    for joint_index in range(0,4):
        # plt.plot(data_frame_tr['q_'+str(joint_index+1)].values)
        # check acc
        current_max_acc = np.abs(data_frame_tr['ddq_'+str(joint_index+1)].values).max()
        if current_max_acc>max_acc[joint_index]:
            max_acc[joint_index] = current_max_acc
        # check vel
        current_max_vel = np.abs(data_frame_tr['dq_'+str(joint_index+1)].values).max()
        if current_max_vel>max_vel[joint_index]:
            max_vel[joint_index] = current_max_vel
    # plt.show()
    # update ouput var
    mean_var_output += data_frame_tr[ouput_labels].values.var(0)
    # add noise
    if flg_add_noise:
        # plt.plot(data_frame_tr[ouput_labels].values)
        data_frame_tr[ouput_labels] = data_frame_tr[ouput_labels].values+\
                                      std_noise*np.random.randn(*data_frame_tr[ouput_labels].values.shape)
        # plt.plot(data_frame_tr[ouput_labels].values)
        # plt.show()
        data_frame_test[ouput_labels] = data_frame_test[ouput_labels].values+\
                                        std_noise*np.random.randn(*data_frame_test[ouput_labels].values.shape)
        # save noisy data
        common_path_noisy_data = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path_noisy+str(run)
        tr_path_noisy_data = data_path+common_path_noisy_data+'_data_tr.pkl'
        pkl.dump(data_frame_tr, open(tr_path_noisy_data, 'wb'))
        test_path_noisy_data = data_path+common_path_noisy_data+'_data_test.pkl'
        pkl.dump(data_frame_test, open(test_path_noisy_data, 'wb'))
# update mean var
mean_var_output = mean_var_output/num_run
# print stats
print('max acc: ', max_acc)
print('max vel: ', max_vel)
print('mean var output: ', mean_var_output)
