"""Print the number of monomials of the polynomials space
defined for the SCARA and the UR10, together with the number of parameters
of the structural network model
Author: Alberto Dalla Libera"""

import StructuralNetwork_models

# SCARA robot
num_rev = 3
num_prism = 1
SCARA_num_B_elements = (4**2-4)/2+4
SCARA_deg_dict = StructuralNetwork_models.get_structural_info(num_rev, num_prism)

# UR10 robot
num_rev = 6
num_prism = 0
UR10_num_B_elements = (6**2-6)/2+6
UR10_deg_dict = StructuralNetwork_models.get_structural_info(num_rev, num_prism)

print('\n SCARA robot:')
print('-num monomials: '+str(SCARA_deg_dict['coef_deg_2n'].shape[0]))
print('-num par network: '+str(SCARA_num_B_elements*SCARA_deg_dict['coef_deg_2n'].shape[0]+SCARA_deg_dict['coef_deg_n'].shape[0]))

print('\n UR10 robot:')
print('-num monomials: '+str(UR10_deg_dict['coef_deg_2n'].shape[0]))
print('-num par network: '+str(UR10_num_B_elements*UR10_deg_dict['coef_deg_2n'].shape[0]+UR10_deg_dict['coef_deg_n'].shape[0]))
