"""Estimator of the inverse dynamics of the UR10 robot based 
on a 2 hidden layer neural network
Author: Alberto Dalla Libera
"""
import Project_Utils
import torch
import torch.utils.data
import Models
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time


#### SET PARAMETERS PARAMETERS
print('---Set parameters')
flg_load = True
flg_train = False
flg_save = True
flg_norm = False
# set type and device
dtype=torch.float64
flg_cuda = False
flg_estimates = True
# data path
tr_path_list = ['UR10_data/dataset_200_points_no_load_with_phi.pkl']
test_path_list = ['UR10_data/dataset_50_points_no_load_with_phi.pkl',
                  'UR10_data/dataset_circle_r30_vel250_with_phi.pkl']
# saving and loading path
saving_model_path = 'Results/UR10/Models/NN_sigmoid_UR10_40tr.pt'
loading_model_path = 'Results/UR10/Models/NN_sigmoid_UR10_40tr.pt'
# data loader parameters
shuffle = True
batch_size = 200
downsampling_tr = 1
downsampling_test = 1
# optimization parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.01, weight_decay=0)
N_epoch = 101
N_epoch_print = 5


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
#set the joint to be considered
num_dof = 6
joint_index_list = np.arange(0,num_dof)
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0]*num_dof
output_feature = 'i'


#### LOAD THE DATASET
print('---Load data and get the dataset')
q_names = ['q_'+joint_name for joint_name in joint_names]
dq_names = ['dq_'+joint_name for joint_name in joint_names]
ddq_names = ['ddq_'+joint_name for joint_name in joint_names]
input_features = q_names+dq_names+ddq_names
input_features_joint_list = [input_features]*num_dof
output_feature = 'i'
# training dataset
input_tr_list = []
output_tr_list = []
for tr_path in tr_path_list:
    input_tr_tmp, output_tr_tmp, active_dims_list = Project_Utils.get_data_from_features(tr_path, input_features, input_features_joint_list, output_feature, num_dof)
    input_tr_list.append(input_tr_tmp)
    output_tr_list.append(output_tr_tmp)
input_tr = np.concatenate(input_tr_list,0)
output_tr = np.concatenate(output_tr_list,0)
# test dataset
input_test_list = []
output_test_list = []
for test_path in test_path_list:
    input_test_tmp, output_test_tmp, _ = Project_Utils.get_data_from_features(test_path, input_features, input_features_joint_list, output_feature, num_dof)
    input_test_list.append(input_test_tmp)
    output_test_list.append(output_test_tmp)
input_test = np.concatenate(input_test_list,0)
output_test = np.concatenate(output_test_list,0)
# normaliziation
if flg_norm:
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# downsampling
input_tr = input_tr[0::downsampling_tr,:]
output_tr = output_tr[0::downsampling_tr,:]
input_test = input_test[0::downsampling_test,:]
output_test = output_test[0::downsampling_test,:]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)

#### GET THE MODEL
print('---Get the model')
# set the number of hidden layers
num_unit_1_list = [600]*num_dof
num_unit_2_list = [600]*num_dof
# set the device
if flg_cuda:
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')
# initialize the model
m = Models.m_indep_NN_sigmoid(num_dof=num_dof,
                                 num_input=num_dof*3,
                                 num_unit_1_list=num_unit_1_list,
                                 num_unit_2_list=num_unit_2_list,
                                 name='NN_torque_model',
                                 dtype=dtype,
                                 device=device)
# move the model to the device
m.to(device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(1)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(loading_model_path))
# train the model
if flg_train:
    m.train_model(joint_index_list=joint_index_list,
                  X=input_tr,
                  Y=output_tr, 
                  criterion=torch.nn.MSELoss(),
                  f_optimizer=f_optimizer,
                  batch_size=batch_size,
                  shuffle=shuffle,
                  N_epoch=N_epoch,
                  N_epoch_print=N_epoch_print)
# save the model
print('Save the model...')
if flg_save:
    torch.save(m.state_dict(), saving_model_path)


if flg_estimates:
    #### GET THE ESTIMATE
    print('---Get estimate')
    with torch.no_grad():
        print('Training estimate...')
        Y_tr_hat_list = m.get_torque_estimates(X_test=input_tr,
                                               joint_indices_list=joint_index_list)
        print('Test estimate...')
        Y_test_hat_list = m.get_torque_estimates(X_test=input_test,
                                                 joint_indices_list=joint_index_list)


    #### PRINT ESTIMATE AND STATS
    Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                                   output_test=output_test,
                                                                                   Y_tr_hat_list=Y_tr_hat_list,
                                                                                   Y_test_hat_list=Y_test_hat_list,
                                                                                   flg_norm=flg_norm,
                                                                                   norm_coeff=norm_coeff,
                                                                                   joint_index_list=joint_index_list,
                                                                                   output_feature=output_feature,
                                                                                   var_tr_list=None,
                                                                                   var_test_list=None)
    # print the estimates
    Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=False, output_feature=output_feature)
    Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=False, output_feature=output_feature)
    plt.show()
    #get the erros stats
    Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
    Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)