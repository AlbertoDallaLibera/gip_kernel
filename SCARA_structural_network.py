"""Implementation of the method proposed in Structural Network modeling and control of rigid body robots
   for the simulated scara robot
Author: Alberto Dalla Libera"""

import sys
sys.path.append('..')
import Project_Utils
import torch
import torch.utils.data
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import argparse
import pickle as pkl
import StructuralNetwork_models

# Set arguments
p = argparse.ArgumentParser('SCARA robot structural_network estimator')
p.add_argument('-data_path',
               type=str,
               default='SCARA_data/data_with_kin_error_omega2_2000/rand_U',
               help='Data path')
p.add_argument('-model_path',
               type=str,
               default='Results/Kin_error_omega2/',
               help='models path')
p.add_argument('-additional_path',
               type=str,
               default='_with_noise_run0',
               help='additional_path')
p.add_argument('-flg_load',
               type=bool,
               default=False,
               help='flg load model')
p.add_argument('-flg_save',
               type=bool,
               default=False,
               help='flg save model')
p.add_argument('-flg_train',
               type=bool,
               default=False,
               help='flg train ')
p.add_argument('-std_noise_kin_lin',
               type=int,
               default=5,
               help='noise in distances')
p.add_argument('-std_noise_kin_ang',
               type=int,
               default=5,
               help='noise in angles')
p.add_argument('-max_omega',
               type=float,
               default=2.,
               help='max omega')
p.add_argument('-batch_size',
               type=int,
               default=1000,
               help='batch_size')
p.add_argument('-shuffle',
               type=bool,
               default=False,
               help='shuffle')
p.add_argument('-flg_norm',
               type=bool,
               default=False,
               help='Normalize signal')
p.add_argument('-N_epoch',
               type=int,
               default=51,
               help='num epoch')
p.add_argument('-N_epoch_print',
               type=int,
               default=51,
               help='num epoch print')
p.add_argument('-flg_cuda',
               type=bool,
               default=False,
               help='set the device type')
p.add_argument('-num_dat_tr',
               type=int,
               default=None,
               help='number of data to use in the training')
locals().update(vars(p.parse_known_args()[0]))


#### SET FILE PARAMETERS
print('---Set the file parameters')
# loading path
common_path = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path
tr_path = data_path+common_path+'_data_tr.pkl'
test_path = data_path+common_path+'_data_test.pkl'
print('tr_path',tr_path)
print('test_path',test_path)
# saving and loading path
model_saving_path = model_path+'Models/SN'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
model_loading_path = model_path+'Models/SN'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
estimate_tr_saving_path = model_path+'Estimates/SN'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_tr_hat.pt'
estimate_test_saving_path = model_path+'Estimates/SN'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'


#### SET OPTIMIZATION PARAMETERS
print('---Set the optimization')
# data loader parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.05, weight_decay=1.)
# f_optimizer = lambda p:torch.optim.SGD(p, lr=0.1, weight_decay=0)
# f_optimizer = lambda p: torch.optim.RMSprop(p, lr=0.1, alpha=0.99, eps=1e-08, weight_decay=0, momentum=0, centered=False)
# set type
dtype=torch.float64


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
# set the joint to be considered
num_dof = 4
joint_index_list = range(0,num_dof)
# link_index_list = [0]
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0,0,1,0]
output_feature = 'tau'


#### LOAD THE DATASET
print('---Load data and get the dataset')
q_names = ['q_'+joint_name for joint_name in joint_names]
dq_names = ['dq_'+joint_name for joint_name in joint_names]
ddq_names = ['ddq_'+joint_name for joint_name in joint_names]
input_features = q_names+dq_names+ddq_names
input_features_joint_list = [input_features]*num_dof
#training
input_tr, output_tr, _ = Project_Utils.get_data_from_features(tr_path,
                                                              input_features,
                                                              input_features_joint_list,
                                                              output_feature,
                                                              num_dof)
#test
input_test, output_test, _ = Project_Utils.get_data_from_features(test_path,
                                                                  input_features,
                                                                  input_features_joint_list,
                                                                  output_feature,
                                                                  num_dof)
# normalization
if flg_norm:
    print('Normalize signals...')
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# select the subset of training data to use
input_tr = input_tr[:num_dat_tr]
output_tr = output_tr[:num_dat_tr]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET STRUCTURAL NETWORK OBJECT MODEL
print('----Get the structural_network object model')
num_rev = 3
num_prism = 1
deg_dict = StructuralNetwork_models.get_structural_info(num_rev, num_prism)
rev_indices = [0,1,3]
prism_indices = [2]
num_monomials_2n = deg_dict['coef_deg_2n'].shape[0]
num_monomials_n = deg_dict['coef_deg_n'].shape[0]
dtype=torch.float64
device=torch.device('cpu')
m = StructuralNetwork_models.Structural_network(deg_dict=deg_dict, 
                                                num_rev=num_rev,
                                                num_prism=num_prism,
                                                rev_indices=rev_indices,
                                                prism_indices=prism_indices,
                                                num_monomials_2n=num_monomials_2n,
                                                num_monomials_n=num_monomials_n,
                                                dtype=dtype, device=device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(8)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(model_loading_path))
# train the model
if flg_train:
    print('Train the model...')
    m.train_model(X=input_tr,
                  Y=output_tr, 
                  criterion=torch.nn.MSELoss(),
                  optimizer=f_optimizer(m.parameters()),
                  batch_size=batch_size,
                  shuffle=shuffle,
                  N_epoch=N_epoch,
                  N_epoch_print=N_epoch_print)
# save the model
if flg_save:
    print('Save the model...')
    torch.save(m.state_dict(), model_saving_path)



#### GET THE ESTIMATE
print('---Get estimate')
with torch.no_grad():
    print('Training estimate...')
    Y_tr_hat = m.get_estimates(input_tr)
    Y_tr_hat_list = [Y_tr_hat[:,joint_index:joint_index+1] for joint_index in range(0, num_dof)]
    print('Test estimate...')
    Y_test_hat = m.get_estimates(input_test)
    Y_test_hat_list = [Y_test_hat[:,joint_index:joint_index+1] for joint_index in range(0, num_dof)]


#### PRINT ESTIMATE AND STATS
Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                               output_test=output_test,
                                                                               Y_tr_hat_list=Y_tr_hat_list,
                                                                               Y_test_hat_list=Y_test_hat_list,
                                                                               flg_norm=flg_norm,
                                                                               norm_coeff=norm_coeff,
                                                                               joint_index_list=joint_index_list,
                                                                               output_feature=output_feature,
                                                                               var_tr_list=None,
                                                                               var_test_list=None)
# save estimates
if flg_save:
    pkl.dump(Y_tr_hat_pd, open(estimate_tr_saving_path, 'wb'))
    pkl.dump(Y_test_hat_pd, open(estimate_test_saving_path, 'wb'))
# print the estimates
Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
plt.show()
#get the erros stats
Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)