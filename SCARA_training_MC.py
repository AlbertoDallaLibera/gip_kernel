"""Experimetn 1.1: Monte Carlo simulation
Author: Alberto Dalla Libera
"""
import os

num_run = 20
run_indices = range(0,num_run)
std_noise_kin_lin = 5
std_noise_kin_ang = 5
max_omega = 2.
num_dat_tr = 2000
additional_path = '_with_noise_run'

# #### GPR estimators optimization parameters
# model_name_list = ['SP', 'PP','GIP', 'RBF']
# N_epoch_list = [401, 101, 401, 901]
# batch_size = 200

# #### NN_sigmoid estimator optimization parameters (set flg cuda to true if a GPU is available)
# model_name_list = ['NN_sigmoid']
# N_epoch_list = [1201]
# batch_size = 100

# #### Fisherian estimator 
# model_name_list = ['FE']
# N_epoch_list = [1] #not relevant (no hyperparameters opt needed)
# batch_size = 0 #not relevant (no hyperparameters opt needed)

# #### Structural network estimator (set cuda to true if a GPU is available)
# model_name_list = ['SN']
# N_epoch_list = [1000] 
# batch_size = 0 #not relevant, we have found that using all the data is the best solution

data_path = 'SCARA_data/data_with_kin_error_omega2_2000/rand_U'
model_path = 'Results/SCARA/Kin_error_omega2_2000/'


for model_index, model_name in enumerate(model_name_list):
    N_epoch = N_epoch_list[model_index]
    for run in run_indices:
        additional_path_run = additional_path+str(run)
        str_command = 'python SCARA_'+model_name+'.py'+\
                      ' -std_noise_kin_lin '+str(std_noise_kin_lin)+\
                      ' -std_noise_kin_ang '+str(std_noise_kin_ang)+\
                      ' -max_omega '+str(max_omega)+\
                      ' -additional_path '+additional_path_run+\
                      ' -data_path '+data_path+\
                      ' -model_path '+model_path+\
                      ' -num_dat_tr '+str(num_dat_tr)+\
                      ' -batch_size '+str(batch_size)+\
                      ' -N_epoch ' +str(N_epoch)+\
                      ' -shuffle True'+\
                      ' -flg_train True'+\
                      ' -flg_save True'
        print('\n##########\nstr_command: '+ str_command+'\n##########')
        os.system(str_command)