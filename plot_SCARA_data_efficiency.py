"""Data efficiency plot
Author: Alberto Dalla Libera
"""
import pandas as pd
import Project_Utils
import numpy as np
import matplotlib.pyplot as plt
import Project_Utils
import matplotlib
import sys

def get_stats(data, data_est):
    mean_list = []
    var_list = []
    for joint in range(0,4):
        label = 'tau_'+str(joint+1)
        label_est = 'tau_est_'+str(joint+1)
        squared_error  = (data[label].values-data_est[label_est].values)**2
        mean_list.append(squared_error.mean())
        var_list.append(squared_error.var())
    return mean_list, var_list


# folders path
data_path ='SCARA_data/data_no_kin_noise/'
results_path = 'Results/SCARA/No_kin_error/Estimates/'
additional_path = '_run'
additional_path_hat = '_with_noise_run'

# file parameters
num_run = 1
max_omega = 2.
std_noise_kin_lin = 0
std_noise_kin_ang = 0
num_dat_tr_list = range(200,4200,200)
# model_names = ['PP', 'GIP', 'RBF', 'NN_sigmoid']
model_names = ['PP', 'GIP', 'RBF', 'NN_sigmoid']
# init the list with results
SCARA_means_list = [[[] for i in range(0,len(num_dat_tr_list))]
                    for model in model_names]
SCARA_vars_list = [[[] for i in range(0,len(num_dat_tr_list))]
                    for model in model_names]
# get stats from data
for num_dat_tr_index, num_dat_tr in enumerate(num_dat_tr_list):
    # iterate over the run
    for run in range(0,num_run):
        # additionals names
        additional_name = additional_path+str(run)
        experiment_name = str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_name
        additional_name_hat = additional_path_hat+str(run)
        experiment_name_hat = str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_name_hat
        # target data
        current_data_path = data_path+'rand_U_'+experiment_name+'_data_test.pkl'
        data = pd.read_pickle(current_data_path)
        # iterate over the possible models
        results_additional_name = experiment_name_hat+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'
        for model_index, model_name in enumerate(model_names):
            # load estimates
            results_path_model = results_path+model_name+'_'+results_additional_name
            data_est = pd.read_pickle(results_path_model)
            # get stats
            mean_run  , var_run = get_stats(data, data_est)
            # append stats
            SCARA_means_list[model_index][num_dat_tr_index].append(np.array([mean_run]))
            SCARA_vars_list[model_index][num_dat_tr_index].append(np.array([var_run]))
# extract the mean of the MSE over the different runs
m = [np.concatenate([(np.concatenate(mean_list,0).mean(0)).reshape([1,-1])
                     for mean_list in SCARA_mean_list_model], 0)
     for SCARA_mean_list_model in SCARA_means_list]
for model_index, model_name in enumerate(model_names):
    print(model_name,m[model_index])
# compute the global error
cum_error = [mean_errors.sum(1) for mean_errors in m]
for model_index, model_name in enumerate(model_names):
    print(model_name,cum_error[model_index])


# sys.exit()

# matplotlib parameters
font = {'family' : 'normal',
        'size'   : 12}
#        'weight' : 'bold',
matplotlib.rc('font', **font)
matplotlib.rcParams['pdf.fonttype']=42
matplotlib.rcParams['ps.fonttype']=42
# colors
colors = ['#006600', '#0066cc', '#b30000', '#666633']
# y_ticks
min_exp = -7
max_exp = 5
ticks_exponent = range(max_exp,min_exp,-1)
y_ticks = [10**k for k in ticks_exponent]
y_ticks_names = ['' for i in y_ticks]
for k in range(0,len(y_ticks), 2):
    y_ticks_names[k] = '$10^{'+str(ticks_exponent[k])+'}$'
# x_ticks
x_ticks_names = ['' for tick in num_dat_tr_list]
for i in range(0,len(x_ticks_names),3):
    x_ticks_names[i] = str(num_dat_tr_list[i])

fig = plt.figure()
plt.grid()
plt.ylabel('GMSE', fontsize=12)
plt.plot(num_dat_tr_list, cum_error[0],label='$PP$', color=colors[0],linewidth=2.)
plt.plot(num_dat_tr_list, cum_error[1],label='$GIP$',color=colors[1],linewidth=2.)
plt.plot(num_dat_tr_list, cum_error[2],label='$RBF$', color=colors[2],linewidth=2.)
plt.plot(num_dat_tr_list, cum_error[3],label='$NN$', color=colors[3],linewidth=2.)
plt.yscale('log')
plt.legend(ncol=4, loc='bottom right',handlelength=2., handleheight=2.,handletextpad=.1, columnspacing=0.2, fontsize=12, borderaxespad=0.1, borderpad=0.1)
plt.xticks(num_dat_tr_list)
# ax.tick_params(labelbottom=False)
plt.xlim(num_dat_tr_list[0],num_dat_tr_list[-1])
plt.ylim(top=10**max_exp, bottom=10**min_exp)
plt.yticks(y_ticks, y_ticks_names, fontsize=12)
plt.xticks(num_dat_tr_list, x_ticks_names)
plt.xlabel('N training data', fontsize=12)
plt.savefig('SCARA_data_efficiency_cumulative_error.pdf', bbox_inches='tight')
plt.show()