"""Modelbased estimator of the inverse dynamics of the UR10 robot
Author: Alberto Dalla Libera
"""
import Project_Utils
import torch
import torch.utils.data
import gpr_lib.Utils.Parameters_covariance_functions as cov_functions
import gpr_lib.Likelihood.Gaussian_likelihood as Likelihood
import Models
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import pickle as pkl


#### SET PARAMETERS PARAMETERS
print('---Set parameters')
flg_load = True
flg_train = False
flg_save = True
flg_norm = False
# set type and device
dtype=torch.float64
flg_cuda = False
flg_estimates = True
# data path
tr_path_list = ['UR10_data/dataset_200_points_no_load_with_phi.pkl']
test_path_list = ['UR10_data/dataset_50_points_no_load_with_phi.pkl',
                  'UR10_data/dataset_circle_r30_vel250_with_phi.pkl']
# saving and loading path
model_saving_path = 'Results/UR10/Models/PP_UR10_40tr.pt'
model_loading_path = 'Results/UR10/Models/PP_UR10_40tr.pt'
estimate_tr_saving_path = 'Results/UR10/Estimates/PP_data_tr_hat.pt' 
estimate_test_saving_path = 'Results/UR10/Estimates/PP_data_test_hat.pt' 
# data loader parameters
shuffle = True
batch_size = 200
downsampling_tr = 1
downsampling_test = 1
max_input_loc = 4000
# optimization parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.01, weight_decay=0)
N_epoch = 11
N_epoch_print = 5


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
#set the joint to be considered
num_dof = 6
joint_index_list = np.arange(0,num_dof)
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0]*num_dof
output_feature = 'i'

#### LOAD THE DATASET
print('---Load data and get the dataset')
num_phi_nominal = 62
input_features_joint_list = [['phi_nominal_'+str(joint_index)+'_'+str(phi_index) for phi_index in range(0,num_phi_nominal)]
                             for joint_index in range(0,num_dof)]
input_features=[]
for input_features_joint in input_features_joint_list:
    input_features += input_features_joint
# training dataset
input_tr_list = []
output_tr_list = []
for tr_path in tr_path_list:
    input_tr_tmp, output_tr_tmp, active_dims_list = Project_Utils.get_data_from_features(tr_path, input_features, input_features_joint_list, output_feature, num_dof)
    input_tr_list.append(input_tr_tmp)
    output_tr_list.append(output_tr_tmp)
input_tr = np.concatenate(input_tr_list,0)
output_tr = np.concatenate(output_tr_list,0)
# test dataset
input_test_list = []
output_test_list = []
for test_path in test_path_list:
    input_test_tmp, output_test_tmp, _ = Project_Utils.get_data_from_features(test_path, input_features, input_features_joint_list, output_feature, num_dof)
    input_test_list.append(input_test_tmp)
    output_test_list.append(output_test_tmp)
input_test = np.concatenate(input_test_list,0)
output_test = np.concatenate(output_test_list,0)
# normaliziation
if flg_norm:
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# downsampling
input_tr = input_tr[0::downsampling_tr,:]
output_tr = output_tr[0::downsampling_tr,:]
input_test = input_test[0::downsampling_test,:]
output_test = output_test[0::downsampling_test,:]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET THE MODEL
print('---Get the model')
# set noise initialization
sigma_n_init_list = [ np.array([1.]) for joint in range(0, num_dof)]
# Sigma par
Sigma_function_list = [ cov_functions.diagonal_covariance for joint in range(0,num_dof)]
flg_ARD = True
Sigma_f_additional_par_list = [ [len(active_dims_link), flg_ARD] for active_dims_link in active_dims_list]
Sigma_pos_par_init_list = [np.ones(additional_par_link[0]) for additional_par_link in Sigma_f_additional_par_list]
# downsampling mode
downsampling_mode='Downsampling'
# set the device
if flg_cuda:
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')
# init the model
m = Models.m_ind_LIN(num_dof, active_dims_list,
                     sigma_n_init_list=sigma_n_init_list, flg_train_sigma_n=True,
                     Sigma_function_list=Sigma_function_list, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                     Sigma_pos_par_init_list=Sigma_pos_par_init_list, flg_train_Sigma_pos_par=True,
                     Sigma_free_par_init_list=None, flg_train_Sigma_free_par=False,
                     name='PP_UR10', dtype=torch.float64, max_input_loc=max_input_loc)
# move the model to the device
m.to(device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(1)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(model_loading_path))
# train the joint minimizing the negative MLL
if flg_train:
    print('Train the model with minimizing the negative MLL...')
    m.train_model(joint_index_list=joint_index_list,
                  X=input_tr, Y=output_tr,
                  criterion = Likelihood.Marginal_log_likelihood(), f_optimizer=f_optimizer,
                  batch_size=batch_size, shuffle = shuffle,
                  N_epoch=N_epoch, N_epoch_print=N_epoch_print)
# save the model
print('Save the model...')
if flg_save:
    torch.save(m.state_dict(), model_loading_path)


if flg_estimates:
    #### GET THE ESTIMATE
    print('---Get estimate')
    with torch.no_grad():
        print('Training estimate...')
        t_start = time.time()
        Y_tr_hat_list, var_tr_list, alpha_tr_list, m_X_list, K_X_inv_list, indices_list = m.get_torque_estimates(input_tr, output_tr, input_tr,
                                                                                                                 joint_indices_list=joint_index_list,
                                                                                                                 flg_return_K_X_inv=True)
        t_stop = time.time()
        print('Time elapsed ', t_stop-t_start)
        print('Test estimate...')
        t_start = time.time()
        Y_test_hat_list, var_test_list, _, _, _, _ = m.get_torque_estimates(input_tr, output_tr, input_test,
                                                                            alpha_list_par=alpha_tr_list, m_X_list_par=m_X_list,
                                                                            indices_list=indices_list,
                                                                            joint_indices_list=joint_index_list,
                                                                            K_X_inv_list_par=K_X_inv_list)
        t_stop = time.time()
        print('Time elapsed ', t_stop-t_start)


    #### PRINT ESTIMATE AND STATS
    Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                                   output_test=output_test,
                                                                                   Y_tr_hat_list=Y_tr_hat_list,
                                                                                   Y_test_hat_list=Y_test_hat_list,
                                                                                   flg_norm=flg_norm,
                                                                                   norm_coeff=norm_coeff,
                                                                                   joint_index_list=joint_index_list,
                                                                                   output_feature=output_feature,
                                                                                   var_tr_list=var_tr_list,
                                                                                   var_test_list=var_test_list)
    # print the estimates
    Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
    Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
    plt.show()
    #get the erros stats
    Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
    Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)