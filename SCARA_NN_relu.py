"""Learning of the SCARA inverse dynamics based on a 
fully connected NN with relu as activation function
Author: Alberto Dalla Libera
"""
import sys
sys.path.append('..')
import Project_Utils
import torch
import torch.utils.data
import Models
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import argparse
import pickle as pkl

# Set arguments
p = argparse.ArgumentParser('SCARA robot NN_relu estimator')
p.add_argument('-data_path',
               type=str,
               default='SCARA_data/data_with_kin_error_omega1/rand_U',
               help='Data path')
p.add_argument('-model_path',
               type=str,
               default='Results/Kin_error_omega1/',
               help='models path')
p.add_argument('-additional_path',
               type=str,
               default='_run0',
               help='additional_path')
p.add_argument('-flg_load',
               type=bool,
               default=False,
               help='flg load model')
p.add_argument('-flg_save',
               type=bool,
               default=False,
               help='flg save model')
p.add_argument('-flg_train',
               type=bool,
               default=False,
               help='flg train ')
p.add_argument('-std_noise_kin_lin',
               type=int,
               default=0,
               help='noise in distances')
p.add_argument('-std_noise_kin_ang',
               type=int,
               default=0,
               help='noise in angles')
p.add_argument('-max_omega',
               type=float,
               default=1.,
               help='max omega')
p.add_argument('-batch_size',
               type=int,
               default=1000,
               help='batch_size')
p.add_argument('-shuffle',
               type=bool,
               default=False,
               help='shuffle')
p.add_argument('-flg_norm',
               type=bool,
               default=False,
               help='Normalize signal')
p.add_argument('-N_epoch',
               type=int,
               default=51,
               help='num epoch')
p.add_argument('-flg_cuda',
               type=bool,
               default=False,
               help='set the device type')
p.add_argument('-num_dat_tr',
               type=int,
               default=None,
               help='number of data to use in the training')
locals().update(vars(p.parse_known_args()[0]))


#### SET FILE PARAMETERS
print('---Set the file parameters')
# loading path
common_path = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path
tr_path = data_path+common_path+'_data_tr.pkl'
test_path = data_path+common_path+'_data_test.pkl'
print('tr_path',tr_path)
print('test_path',test_path)
# saving and loading path
model_saving_path = model_path+'Models/NN_relu'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
model_loading_path = model_path+'Models/NN_relu'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
estimate_tr_saving_path = model_path+'Estimates/NN_relu'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_tr_hat.pt'
estimate_test_saving_path = model_path+'Estimates/NN_relu'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'


#### SET OPTIMIZATION PARAMETERS
print('---Set the optimization')
# data loader parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.001, weight_decay=0)
N_epoch_print = 10
# set type
dtype=torch.float64


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
# set the joint to be considered
num_dof = 4
joint_index_list = range(0,num_dof)
# link_index_list = [0]
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0,0,1,0]
output_feature = 'tau'


#### LOAD THE DATASET
print('---Load data and get the dataset')
q_names = ['q_'+joint_name for joint_name in joint_names]
dq_names = ['dq_'+joint_name for joint_name in joint_names]
ddq_names = ['ddq_'+joint_name for joint_name in joint_names]
input_features = q_names+dq_names+ddq_names
input_features_joint_list = [input_features]*num_dof
#training
input_tr, output_tr, _ = Project_Utils.get_data_from_features(tr_path,
                                                              input_features,
                                                              input_features_joint_list,
                                                              output_feature,
                                                              num_dof)
#test
input_test, output_test, _ = Project_Utils.get_data_from_features(test_path,
                                                                  input_features,
                                                                  input_features_joint_list,
                                                                  output_feature,
                                                                  num_dof)
# normalization
if flg_norm:
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# select the subset of training data to use
input_tr = input_tr[:num_dat_tr]
output_tr = output_tr[:num_dat_tr]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET THE MODEL
print('---Get the model')
# set the number of hidden layers
num_unit_1_list = [200]*num_dof
num_unit_2_list = [200]*num_dof
# set the device
if flg_cuda:
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')
# initialize the model
m = Models.m_indep_NN_relu(num_dof=num_dof,
                           num_input=num_dof*3,
                           num_unit_1_list=num_unit_1_list,
                           num_unit_2_list=num_unit_2_list,
                           name='NN_torque_model',
                           dtype=dtype,
                           device=device)
# move the model to the device
m.to(device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(1)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(model_loading_path))
# train the model
if flg_train:
    m.train_model(joint_index_list=joint_index_list,
                  X=input_tr,
                  Y=output_tr, 
                  criterion=torch.nn.MSELoss(),
                  f_optimizer=f_optimizer,
                  batch_size=batch_size,
                  shuffle=shuffle,
                  N_epoch=N_epoch,
                  N_epoch_print=N_epoch_print)
# save the model
print('Save the model...')
if flg_save:
    torch.save(m.state_dict(), model_saving_path)


#### GET THE ESTIMATE
print('---Get estimate')
with torch.no_grad():
    print('Training estimate...')
    Y_tr_hat_list = m.get_torque_estimates(X_test=input_tr,
                                           joint_indices_list=joint_index_list)
    print('Test estimate...')
    Y_test_hat_list = m.get_torque_estimates(X_test=input_test,
                                             joint_indices_list=joint_index_list)


#### PRINT ESTIMATE AND STATS
Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                               output_test=output_test,
                                                                               Y_tr_hat_list=Y_tr_hat_list,
                                                                               Y_test_hat_list=Y_test_hat_list,
                                                                               flg_norm=flg_norm,
                                                                               norm_coeff=norm_coeff,
                                                                               joint_index_list=joint_index_list,
                                                                               output_feature=output_feature,
                                                                               var_tr_list=None,
                                                                               var_test_list=None)
# save estimates
if flg_save:
    pkl.dump(Y_tr_hat_pd, open(estimate_tr_saving_path, 'wb'))
    pkl.dump(Y_test_hat_pd, open(estimate_test_saving_path, 'wb'))
# # print the estimates
# Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# plt.show()
#get the erros stats
Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)