"""Test of the implementation of the method proposed in Structural Network modeling and control of rigid body robots
Author: Alberto Dalla Libera"""

import Project_Utils
import torch
import torch.utils.data
import numpy as np
import matplotlib.pyplot as plt
import StructuralNetwork_models
import pandas as pd


#### SET PARAMETERS
print('---Set parameters')
flg_train = True
N_epoch = 40
N_epoch_print = 1
batch_size = 100
shuffle = True
# data loader parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.01, weight_decay=0.)
# f_optimizer = lambda p:torch.optim.SGD(p, lr=0.001, weight_decay=0)
# set type
dtype=torch.float64


#### GET DATA
print('---Get data')
time_interval = 20
sampling_time = 0.005
m1, m2, m3, m4, m5 = 1., 1., 1., 1., 1.
f_q1_tr = lambda t: 0.6*np.sin(np.pi*t) 
f_q2_tr = lambda t: 0.6*(np.sin(np.pi*t)-1)
f_q1_test = lambda t: 1*np.sin(np.pi*t) 
f_q2_test = lambda t: 1*(np.sin(np.pi*t)-1)
input_tr = StructuralNetwork_models.get_2dof_trj(time_interval, sampling_time, f_q1_tr, f_q2_tr)
output_tr = StructuralNetwork_models.get_2dof_data(m1, m2, m3, m4, m5, input_tr)
input_test = StructuralNetwork_models.get_2dof_trj(time_interval, sampling_time, f_q1_test, f_q2_test)
output_test = StructuralNetwork_models.get_2dof_data(m1, m2, m3, m4, m5, input_test)
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET STRUCTURAL NETWORK OBJECT MODEL
print('----Get the structural_network object model')
num_rev = 2
num_prism = 0
deg_dict = StructuralNetwork_models.get_structural_info(num_rev, num_prism)
rev_indices = [0,1]
prism_indices = []
num_monomials_2n = deg_dict['coef_deg_2n'].shape[0]
num_monomials_n = deg_dict['coef_deg_n'].shape[0]
dtype=torch.float64
device=torch.device('cpu')
m = StructuralNetwork_models.Structural_network(deg_dict=deg_dict, 
                                                num_rev=num_rev,
                                                num_prism=num_prism,
                                                rev_indices=rev_indices,
                                                prism_indices=prism_indices,
                                                num_monomials_2n=num_monomials_2n,
                                                num_monomials_n=num_monomials_n,
                                                dtype=dtype, device=device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(8)
# train the model
if batch_size is None:
    batch_size = input_tr.shape[0]
if flg_train:
    print('Train the model...')
    m.train_model(X=input_tr,
                  Y=output_tr, 
                  criterion=torch.nn.MSELoss(),
                  optimizer=f_optimizer(m.parameters()),
                  batch_size=batch_size,
                  shuffle=shuffle,
                  N_epoch=N_epoch,
                  N_epoch_print=N_epoch_print)



#### GET THE ESTIMATE
print('---Get estimate')
with torch.no_grad():
    print('Training estimate...')
    Y_tr_hat = m.get_estimates(input_tr)
    print('Test estimate...')
    Y_test_hat = m.get_estimates(input_test)
for joint_index in range(0,2):
    plt.figure()
    plt.plot(output_tr[:,joint_index], label='y')
    plt.plot(Y_tr_hat[:,joint_index], label='y_hat')
    plt.grid()
    plt.title('tau_tr_'+str(joint_index+1))
for joint_index in range(0,2):
    plt.figure()
    plt.plot(output_test[:,joint_index], label='y')
    plt.plot(Y_test_hat[:,joint_index], label='y_hat')
    plt.grid()
    plt.title('tau_test_'+str(joint_index+1))
plt.show()