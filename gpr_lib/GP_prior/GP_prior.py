"""Superclass of the GP objects
Author: Alberto Dalla Libera
"""
import torch
import numpy as np
import time
import matplotlib.pyplot as plt
from .. import Likelihood

class GP_prior(torch.nn.Module):
    """Superclass of GP models:
    Inputs of the models are batch of data X1, X2 with dimension N1xD and N2xD,
    where D is the number of features and N1 and N2 are the number of samples 
    The forward method returns m_X, K_X, K_X_inv, log_det:
    -m_X: mean
    -K_X: covariance matrix
    -K_X_inv: inverse of (K_X+sigma_n**2*I)
    -log_det: logdet of (K_X+sigma_n**2*I)"""


    def __init__(self, active_dims,
                 sigma_n_init=None, flg_train_sigma_n=True,
                 name='', dtype=torch.float64, sigma_n_num=None, device=None):
        """Initialize the object Module objects and save parameters"""
        super(GP_prior,self).__init__()
        self.name = name
        # type and device 
        self.dtype = dtype
        if device is None:
            self.device = torch.device('cpu')
        else:
            self.device = device
        # active dims: tensor with the indices of the features used 
        if active_dims is None:
            self.active_dims = active_dims
        else:
            self.active_dims = torch.tensor(active_dims, requires_grad=False, device=device)
        # set the noise standard deviation
        if sigma_n_init is None:
            self.GP_with_noise = False
        else:
            self.GP_with_noise = True
            self.sigma_n_log = torch.nn.Parameter(torch.tensor(np.log(sigma_n_init), dtype=self.dtype, device=self.device), requires_grad=flg_train_sigma_n)
        # set numerical precision 
        if sigma_n_num is not None:
            self.sigma_n_num = torch.tensor(sigma_n_num, dtype=self.dtype, device=self.device)
        else:
            self.sigma_n_num = torch.tensor(0., dtype=self.dtype, device=self.device)
    

    def to(self, dev):
        """Move the model to the device"""
        super(GP_prior, self).to(dev)
        self.device = dev
        self.sigma_n_num = self.sigma_n_num.to(dev)


    def print_model(self):
        """Print the parameters of the model"""
        print(self.name+' parameters:')
        for par_name, par_value in self.named_parameters():
            print('-', par_name, ':', par_value.data)


    def get_sigma_n_2(self):
        return torch.exp(self.sigma_n_log)**2 + self.sigma_n_num**2
        # return self.sigma_n_log**2 + self.sigma_n_num**2


    def get_estimate(self, X, Y, X_test, Y_test=None, flg_return_K_X_inv=False):
        """Returns the estimate of f in X_test given the observations X Y.
           The function returns also:
           -a vector containing the sigma squared confidence interval
           -the vector of the coefficient"""
        alpha, m_X, K_X_inv = self.get_alpha(X, Y)
        Y_hat, var = self.get_estimate_from_alpha(X, X_test, alpha, m_X, K_X_inv=K_X_inv, Y_test=Y_test)
        if flg_return_K_X_inv:
            return Y_hat, var, alpha, m_X, K_X_inv
        else:
            return Y_hat, var, alpha


    def forward(self, X):
        """Returns:
        -m_X: mean
        -K_X: covariance matrix
        -K_X_inv: inverse of (K_X+sigma_n**2*I)
        -log_det: logdet of (K_X+sigma_n**2*I)"""
        # get the kernel matrix
        N = X.size()[0]
        if self.GP_with_noise:
            K_X = self.get_covariance(X, flg_noise=True) 
        else:
            K_X = self.get_covariance(X)
        # compute the inverse and the logdet with cholesky
        U = torch.cholesky(K_X, upper=True)
        log_det = 2*torch.sum(torch.log(torch.diag(U)))
        U_inv = torch.inverse(U)
        K_X_inv = torch.matmul(U_inv, U_inv.transpose(0,1))
        m_X = self.get_mean(X)
        return m_X, K_X, K_X_inv, log_det


    def forward_for_estimate(self, X):
        """Returns:-m_X: mean
        -K_X: covariance matrix
        -K_X_inv: inverse of (K_X+sigma_n**2*I)
        Inversion is computed with torch.inverse"""
        #get the kernel matrix
        N = X.size()[0]
        if self.GP_with_noise:
            K_X = self.get_covariance(X, flg_noise=True) 
        else:
            K_X = self.get_covariance(X)
        # get the inverse
        K_X_inv = torch.inverse(K_X)
        # get the mean
        m_X = self.get_mean(X)
        return m_X, K_X, K_X_inv


    def get_alpha(self, X, Y):
        """Returns the alpha vector that defines the mean 
        of the posterior distribution"""
        # m_X, _, K_X_inv = self.forward_for_estimate(X)
        m_X, _, K_X_inv, _ = self.forward(X)
        alpha = torch.matmul(K_X_inv, Y-m_X)
        return alpha, m_X, K_X_inv


    def get_estimate_from_alpha(self, X, X_test, alpha, m_X, K_X_inv=None, Y_test=None):
        """Returns the mean of the posterior distribution given the alpha vector""" 
        # get prior covariance and mean
        K_X_test_X = self.get_covariance(X_test, X)
        m_X_test = self.get_mean(X_test) 
        # compute the posterior mean
        Y_hat = m_X_test + torch.matmul(K_X_test_X, alpha)
        if not(Y_test is None):
            print('MSE:',torch.sum( (Y_test-Y_hat)**2 )/Y_test.size()[0])
        # computes the poserior variancve
        if K_X_inv is not None:
            num_test = X_test.size()[0]
            var = self.get_diag_covariance(X_test) - torch.sum(torch.matmul(K_X_test_X, K_X_inv)*(K_X_test_X), dim=1)
            return Y_hat, var
        else:
            return Y_hat


    def get_mean(self, X):
        """Returns the prior mean in X"""
        raise NotImplementedError()


    def get_covariance(self, X1, X2=None, flg_noise=False):
        """Returns the covariance betweeen the input locations X1 X2. 
        If X2 is None X2 is assumed to be equal to X1"""
        raise NotImplementedError()


    def get_diag_covariance(self, X, flg_noise=False):
        """Returns the covariance betweeen the input locations X1 X2. 
        If X2 is None X2 is assumed to be equal to X1"""
        raise NotImplementedError()


    def fit_model(self,trainloader=None, 
                  optimizer=None, criterion=None,
                  N_epoch=1, N_epoch_print=1, flg_time=False, f_saving_model=None, f_print=None):
        """Performs the optimization of the model. The inputs are:
        -A training loader
        -An optimizer
        -N_epoch = number of epoch
        -N_epoch_print = parameter that define how often  print stats"""
        # print initial parametes and initial estimates
        print('\nInitial parameters:')
        self.print_model()  
        # iterate over the training data
        for epoch in range(0,N_epoch):  # loop over the dataset multiple times
            running_loss = 0.0
            N_btc = 0
            optimizer.zero_grad()
            print('\nEPOCH:', epoch)
            for i, data in enumerate(trainloader, 0):
                inputs, labels = data
                # zero the parameter gradients
                optimizer.zero_grad()
                num_btc_opt=0
                # forward + backward + optimize
                t_start = time.time()
                out_GP_priors = self(inputs)
                loss = criterion(out_GP_priors, labels)
                loss.backward(retain_graph=False)
                optimizer.step()
                t_stop = time.time()
                if flg_time:
                    print(t_stop-t_start)
                # update the running loss
                running_loss = running_loss + loss.item()
                N_btc = N_btc + 1
            # print statistics
            if epoch%N_epoch_print==0:
                self.print_model()
                print('Runnung loss:', running_loss/N_btc)
                if f_saving_model is not None:
                    f_saving_model(epoch)
                if f_print is not None:
                    f_print()
        # print final parameters
        print('\nFinal parameters:')
        self.print_model()


    def __add__(self, other_GP_prior):
        """Returns a GP_prior object given by the sum of other_GP_prior"""
        return Sum_Independent_GP(self, other_GP_prior)


    def __mul__(self, other_GP_prior):
        """Returns a GP with mean given by the product of the means and covariance
        given by the element-wise product of the covariances"""
        return Multiply_GP_prior(self, other_GP_prior)




class Combine_GP(GP_prior):
    """Class that extend GP_prior and provide common utilities to multiple kernel operations"""


    def __init__(self, *gp_priors_obj):
        super(Combine_GP, self).__init__(active_dims=None, sigma_n_num=gp_priors_obj[0].sigma_n_num,
                                         dtype=gp_priors_obj[0].dtype, device=gp_priors_obj[0].device)
        # build a list with all the models
        self.gp_list = torch.nn.ModuleList(gp_priors_obj)
        # check the noise flag
        GP_with_noise = False
        for gp in self.gp_list:
            GP_with_noise = GP_with_noise or gp.GP_with_noise 
        self.GP_with_noise = GP_with_noise

    def to(self, dev):
        super(Combine_GP, self).to(dev)
        self.device = dev
        for gp in self.gp_list:
            gp.to(dev)


    def print_model(self):
        """Print the parameters of the model"""
        for gp in self.gp_list:
            gp.print_model()


    def get_sigma_n_2(self):
        sigma_n_2 = torch.zeros(1, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            if gp.GP_with_noise:
                sigma_n_2 += gp.get_sigma_n_2()
        return sigma_n_2




class Sum_Independent_GP(Combine_GP):
    """Class that sum independent GP_priors objects"""


    def __init__(self, *gp_priors_obj):
        super(Sum_Independent_GP, self).__init__(*gp_priors_obj)


    def get_mean(self, X):
        """Returns the sum of the means of the proces in gp_list"""
        N = X.size()[0]
        mean = torch.zeros(N,1, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            mean += gp.get_mean(X)
            return mean


    def get_covariance(self, X1, X2=None, flg_noise=False):
        """Returns the sum of the covariances of the gp_list"""
        N1 = X1.size()[0]
        if X2 is None:
            N2 = N1
        else:
            N2 = X2.size()[0]
        cov = torch.zeros(N1,N2, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            cov += gp.get_covariance(X1,X2, flg_noise=False)
        if flg_noise & self.GP_with_noise:
            cov += self.get_sigma_n_2()*torch.eye(N1, dtype=self.dtype, device=self.device)
        return cov


    def get_diag_covariance(self, X, flg_noise=False):
        diag = torch.zeros(X.size()[0], dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            diag += gp.get_diag_covariance(X, flg_noise=False)
        if flg_noise & self.GP_with_noise:
            diag += self.get_sigma_n_2()
        return diag



class Multiply_GP_prior(Combine_GP):
    """Class that generate a GP_prior multiplying GP_priors objects"""


    def __init__(self, *gp_priors_obj):
        super(Multiply_GP_prior, self).__init__(*gp_priors_obj)


    def get_mean(self, X):
        """Returns the product of the means of the proces in gp_list"""
        N = X.size()[0]
        mean = torch.ones(N,1, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            mean = mean*gp.get_mean(X)
        return mean


    def get_covariance(self, X1, X2=None, flg_noise=False):
        """Returns the product of the covariances of the gp_list"""
        N1 = X1.size()[0]
        if X2 is None:
            N2 = N1
        else:
            N2 = X2.size()[0]
        cov = torch.ones(N1,N2, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            cov *=gp.get_covariance(X1,X2, flg_noise=False)
        if flg_noise & self.GP_with_noise:
            cov +=self.get_sigma_n_2()*torch.eye(N1, dtype=self.dtype, device=self.device)
        return cov


    def get_diag_covariance(self, X, flg_noise=False):
        """Returns the product of the covariances of the gp_list"""
        N = X.size()[0]
        diag = torch.ones(N, dtype=self.dtype, device=self.device)
        for gp in self.gp_list:
            diag *= gp.get_diag_covariance(X, flg_noise=False)
        if flg_noise & self.GP_with_noise:
           diag +=self.get_sigma_n_2()
        return diag