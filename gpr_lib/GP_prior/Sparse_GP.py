"""Sparse GP models (models with non full covariance)
Author: Alberto Dalla Libera
"""
import torch
import numpy as np
from . import GP_prior
from .. import Utils

class Linear_GP(GP_prior.GP_prior):
    """Implementation of the GP with linear kernel (dot product covariance).
       f(X) = phi(X)*w, where the w is defined as a gaussian variable (mean_w and Sigma_w).
       phi(X) is named regression matrix and it is defined throw the activedims.
       The mean and variance of w together with phi define the mean and variance
       of the GP"""


    def __init__(self, active_dims,
                 mean_init=None, flg_mean_trainable=False, flg_no_mean=False,
                 sigma_n_init=None, flg_train_sigma_n=True,
                 Sigma_function=None, Sigma_f_additional_par_list=None, 
                 Sigma_pos_par_init=None, flg_train_Sigma_pos_par=True,
                 Sigma_free_par_init=None, flg_train_Sigma_free_par=True,
                 flg_offset=False,
                 name='',
                 dtype=torch.float64, sigma_n_num=None, device=None):
        super(Linear_GP, self).__init__(active_dims,
                                        sigma_n_init=sigma_n_init, flg_train_sigma_n=flg_train_sigma_n,
                                        name=name, dtype=dtype, sigma_n_num=sigma_n_num, device=device)
        # check active dims
        if active_dims is None:
            raise RuntimeError("Active_dims are needed")
        self.num_features = active_dims.size
        # save flg_offset (flg_offset=True => ones added to the phi)
        self.flg_offset = flg_offset
        # check mean init
        self.check_mean(mean_init, flg_mean_trainable, flg_no_mean)
        # check Sigma init
        self.check_sigma_function(Sigma_function, Sigma_f_additional_par_list,
                                  Sigma_pos_par_init, flg_train_Sigma_pos_par,
                                  Sigma_free_par_init, flg_train_Sigma_free_par)
    

    def check_mean(self, mean_init, flg_mean_trainable, flg_no_mean):
        if mean_init is None:
            mean_init = np.zeros(1)
            flg_no_mean = True
        self.flg_no_mean = flg_no_mean
        self.mean_par = torch.nn.Parameter(torch.tensor(mean_init, dtype=self.dtype, device=self.device), requires_grad=flg_mean_trainable)


    def check_sigma_function(self, Sigma_function, Sigma_f_additional_par_list,
                             Sigma_pos_par_init, flg_train_Sigma_pos_par,
                             Sigma_free_par_init, flg_train_Sigma_free_par):
        if Sigma_function is None:
            raise RuntimeError("Specify a Sigma function")
        self.Sigma_function = Sigma_function
        self.Sigma_f_additional_par_list = Sigma_f_additional_par_list
        if Sigma_pos_par_init is None:
            self.Sigma_pos_par = None
        else:
            self.Sigma_pos_par = torch.nn.Parameter(torch.tensor(np.log(Sigma_pos_par_init), dtype=self.dtype, device=self.device), requires_grad=flg_train_Sigma_pos_par)
        if Sigma_free_par_init is None:
            self.Sigma_free_par = None
        else:
            self.Sigma_free_par = torch.nn.Parameter(torch.tensor(Sigma_free_par_init, dtype=self.dtype, device=self.device), requires_grad=flg_train_Sigma_free_par)


    def get_phi(self,X):
        """Return the regression matrix associated to the inputs X"""
        num_samples = X.shape[0]
        if self.flg_offset:
            return torch.cat([X[:,self.active_dims],torch.ones(num_samples,1, dtype=self.dtype, device=self.device)],1)
        else:
            return X[:,self.active_dims]


    def get_Sigma(self):
        """Computes the Sigma matrix"""
        if self.Sigma_pos_par is None:
            return self.Sigma_function( self.Sigma_pos_par, self.Sigma_free_par, *self.Sigma_f_additional_par_list)
        else:
            return self.Sigma_function( torch.exp(self.Sigma_pos_par), self.Sigma_free_par, *self.Sigma_f_additional_par_list)


    def get_mean(self,X):
        """ Returns phi(X)*w"""
        if self.flg_no_mean:
            N = X.size()[0]
            return torch.zeros(N, 1, dtype=self.dtype, device=self.device)
        else:
            return torch.matmul(self.get_phi(X), self.mean_par)


    def get_covariance(self, X1, X2=None, flg_noise=False):
        """ Returns phi(X)^T*Sigma*phi(X)"""
        # Reconstruct the variance
        Sigma = self.get_Sigma()
        # get the covariance
        phi_X1 = self.get_phi(X1)
        if X2 is None:
            K_X =  torch.matmul(phi_X1, torch.matmul(Sigma, phi_X1.transpose(0,1)))
            # check if we need to add the noise
            if flg_noise & self.GP_with_noise:
                N = X1.size()[0]
                return K_X + self.get_sigma_n_2()*torch.eye(N, dtype=self.dtype, device=self.device)
            else:
                return K_X
        else:
            phi_X2 = self.get_phi(X2)
            return torch.matmul(phi_X1, torch.matmul(Sigma, phi_X2.transpose(0,1)))


    def get_diag_covariance(self, X, flg_noise=False):
        """ Returns the diag of the cov matrix"""
        # get the variance
        Sigma = self.get_Sigma()
        # get the diag of the covariance
        phi_X = self.get_phi(X)
        diag = torch.sum(torch.matmul(phi_X, Sigma)*(phi_X), dim=1)
        if flg_noise & self.GP_with_noise:
            return diag + self.get_sigma_n_2()
        else:
            return diag




class Poly_GP(Linear_GP):
    """GP with polynomial kernel. Implemented extending the Linear_GP"""
    def __init__(self, active_dims, poly_deg,
                 sigma_n_init=None, flg_train_sigma_n=True,
                 Sigma_function=None, Sigma_f_additional_par_list=None, 
                 Sigma_pos_par_init=None, flg_train_Sigma_pos_par=True,
                 Sigma_free_par_init=None, flg_train_Sigma_free_par=True,
                 flg_offset=True,
                 name='', dtype=torch.float64, sigma_n_num=None, device=None):
        # init the linear GP object
        mean_init=None
        flg_mean_trainable=False
        flg_no_mean=True
        super(Poly_GP, self).__init__(active_dims=active_dims,
                                      mean_init=mean_init, flg_mean_trainable=flg_mean_trainable, flg_no_mean=flg_no_mean,
                                      sigma_n_init=sigma_n_init, flg_train_sigma_n=flg_train_sigma_n,
                                      Sigma_function=Sigma_function, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                                      Sigma_pos_par_init=Sigma_pos_par_init, flg_train_Sigma_pos_par=flg_train_Sigma_pos_par,
                                      Sigma_free_par_init=Sigma_free_par_init, flg_train_Sigma_free_par=flg_train_Sigma_free_par,
                                      flg_offset=flg_offset,
                                      name=name, dtype=dtype, sigma_n_num=sigma_n_num, device=device)
        # save the poly deg
        self.poly_deg = poly_deg
    

    def get_covariance(self, X1, X2=None, flg_noise=False):
        """Overwrite the covariance"""
        return (super(Poly_GP, self).get_covariance(X1, X2, flg_noise))**self.poly_deg


    def get_diag_covariance(self, X, flg_noise=False):
        """Overvrite the diag covariance"""
        return (super(Poly_GP, self).get_diag_covariance(X,flg_noise=flg_noise))**self.poly_deg




class MPK_GP(Linear_GP):
    """Implementation of the Multiplicaive Polynomial Kernel"""
    def __init__(self, active_dims, poly_deg,
                 sigma_n_init=None, flg_train_sigma_n=True,
                 Sigma_pos_par_init=None, flg_train_Sigma_pos_par=True,
                 Sigma_free_par_init=None, flg_train_Sigma_free_par=True,
                 flg_offset=True,
                 name='', dtype=torch.float64, sigma_n_num=None, device=None):
        # init the linear GP object
        mean_init=None
        flg_mean_trainable=False
        flg_no_mean=True
        Sigma_function = Utils.Parameters_covariance_functions.diagonal_covariance_semi_def
        Sigma_f_additional_par_list = []
        super(MPK_GP, self).__init__(active_dims=active_dims,
                                      mean_init=mean_init, flg_mean_trainable=flg_mean_trainable, flg_no_mean=flg_no_mean,
                                      sigma_n_init=sigma_n_init, flg_train_sigma_n=flg_train_sigma_n,
                                      Sigma_function=Sigma_function, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                                      Sigma_pos_par_init=None, flg_train_Sigma_pos_par=False,
                                      Sigma_free_par_init=None, flg_train_Sigma_free_par=False,
                                      flg_offset=flg_offset,
                                      name=name, dtype=dtype, sigma_n_num=sigma_n_num, device=device)
        self.poly_deg = poly_deg
        # get kernel parameters
        self.Sigma_free_par = torch.nn.Parameter(torch.tensor(np.log(Sigma_free_par_init),
                                                              dtype=self.dtype,
                                                              device=self.device),
                                                requires_grad=flg_train_Sigma_free_par)
        self.num_Sigma_free_par = int(Sigma_free_par_init.size/poly_deg)
        self.Sigma_pos_par = torch.nn.Parameter(torch.tensor(np.log(Sigma_pos_par_init),
                                                             dtype=self.dtype,
                                                             device=self.device),
                                                requires_grad=flg_train_Sigma_pos_par)
        self.num_Sigma_pos_par = int(Sigma_pos_par_init.size/poly_deg)


    def get_Sigma(self):
        """Computes the Sigma matrix"""
        Sigma_pos_par = torch.exp(self.Sigma_pos_par[self.current_deg*self.num_Sigma_pos_par:(self.current_deg+1)*self.num_Sigma_pos_par])
        Sigma_free_par = torch.zeros(self.num_Sigma_free_par,
                                     dtype=self.dtype,
                                     device=self.device)
        for deg in range(self.current_deg, self.poly_deg):
            Sigma_free_par += torch.exp(self.Sigma_free_par[deg*self.num_Sigma_free_par:(deg+1)*self.num_Sigma_free_par])**2
        Sigma_free_par = torch.sqrt(Sigma_free_par)
        return self.Sigma_function(Sigma_pos_par,
                                   Sigma_free_par,
                                   *self.Sigma_f_additional_par_list)


    def get_covariance(self, X1, X2=None, flg_noise=False):
        N1 = X1.size()[0]
        if X2 is None:
            N2 = N1
        else:
            N2 = X2.size()[0]
        K_X = torch.ones((N1,N2), dtype=self.dtype, device=self.device)
        for deg in range(0,self.poly_deg):
            self.current_deg = deg
            K_X *= super(MPK_GP, self).get_covariance(X1,X2, flg_noise=False)
        if flg_noise & self.GP_with_noise & N1==N2:
            K_X += self.get_sigma_n_2()*torch.eye(N1, dtype=self.dtype, device=self.device)
        return K_X


    def get_diag_covariance(self, X, flg_noise=False):
        """ Returns the diag of the cov matrix"""
        N = X.size()[0]
        diag = torch.ones(N, dtype=self.dtype, device=self.device)
        for deg in range(0,self.poly_deg):
            self.current_deg = deg
            diag *= super(MPK_GP, self).get_diag_covariance(X, flg_noise=False)
        if flg_noise & self.GP_with_noise:
            return diag + self.get_sigma_n_2()
        else:
            return diag