"""
This file contains a collections of functions that returns valid covariance functions
The inputs of these functions is standardize in order to be:
-pos_parameters: a torch tensor that must be positive
-free_par: a torch tensors whose values are unconstrained
-additional_par: additional parameters of the function
Author: Alberto Dalla Libera"""
import numpy as np
import torch


def diagonal_covariance(pos_par=None, free_par=None, num_par=None, flg_ARD=False):
    """Returns a diagonal covariance matrix. if flg_ARD is false all the element alonf the diagonal are equals"""
    if flg_ARD:
        #check dimensions and return the matrix
        if num_par==pos_par.size()[0]:
            return torch.diag(pos_par**2)
        else:
            raise RuntimeError("The number of positive parameters and num_par must be equal when flg_ARD=True")
    else:
        return pos_par**2*torch.eye(num_par, dtype=pos_par.dtype, device=pos_par.device) 


def diagonal_covariance_semi_def(pos_par=None, free_par=None):
    """Returns a diagonal covariance matrix with dimension num_pos_par+num_free_par.
       The firsts elements of the diag are equal to free_par**2, while the last elements
       are equal to pos_par**2
    """
    if pos_par is None:
        pos_par = torch.tensor([], dtype=free_par.dtype, device=free_par.device)
    return torch.diag(torch.cat([free_par, pos_par])**2)