"""Semi parametric estimator of the inverse dynamics of the SCARA robot
Author: Alberto Dalla Libera
"""
import Project_Utils
import torch
import torch.utils.data
import gpr_lib.Utils.Parameters_covariance_functions as cov_functions
import gpr_lib.Likelihood.Gaussian_likelihood as Likelihood
import Models
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import argparse
import pickle as pkl

# Set arguments
p = argparse.ArgumentParser('SCARA robot SP estimator')
p.add_argument('-data_path',
               type=str,
               default='SCARA_data/data_with_kin_error_omega2/rand_U',
               help='Data path')
p.add_argument('-model_path',
               type=str,
               default='Results/Kin_error_omega2/',
               help='models path')
p.add_argument('-additional_path',
               type=str,
               default='_with_noise_run0',
               help='additional_path')
p.add_argument('-flg_load',
               type=bool,
               default=False,
               help='flg load model')
p.add_argument('-flg_save',
               type=bool,
               default=False,
               help='flg save model')
p.add_argument('-flg_train',
               type=bool,
               default=False,
               help='flg train ')
p.add_argument('-std_noise_kin_lin',
               type=int,
               default=4,
               help='noise in distances')
p.add_argument('-std_noise_kin_ang',
               type=int,
               default=4,
               help='noise in angles')
p.add_argument('-max_omega',
               type=float,
               default=3.,
               help='max omega')
p.add_argument('-batch_size',
               type=int,
               default=1000,
               help='batch_size')
p.add_argument('-shuffle',
               type=bool,
               default=False,
               help='shuffle')
p.add_argument('-flg_norm',
               type=bool,
               default=False,
               help='Normalize signal')
p.add_argument('-N_epoch',
               type=int,
               default=51,
               help='num epoch')
p.add_argument('-flg_cuda',
               type=bool,
               default=False,
               help='set the device type')
p.add_argument('-num_dat_tr',
               type=int,
               default=None,
               help='number of data to use in the training')
locals().update(vars(p.parse_known_args()[0]))


#### SET FILE PARAMETERS
print('---Set the file parameters')
# loading path
common_path = '_'+str(std_noise_kin_lin)+'_'+str(std_noise_kin_ang)+'omega'+str(max_omega)+additional_path
tr_path = data_path+common_path+'_data_tr.pkl'
test_path = data_path+common_path+'_data_test.pkl'
# saving and loading path
model_saving_path = model_path+'Models/SP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
model_loading_path = model_path+'Models/SP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'.pt'
estimate_tr_saving_path = model_path+'Estimates/SP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_tr_hat.pt'
estimate_test_saving_path = model_path+'Estimates/SP'+common_path+'_num_dat_tr_'+str(num_dat_tr)+'_data_test_hat.pt'


#### SET OPTIMIZATION PARAMETERS
print('---Set the optimization')
# data loader parameters
f_optimizer = lambda p:torch.optim.Adam(p, lr=0.01, weight_decay=0)
N_epoch_print = 100
# set type
dtype=torch.float64


#### SET THE ROBOT PARAMETERS
print('---Set the robot parameters')
# set the joint to be considered
num_dof = 4
joint_index_list = range(0,num_dof)
# link_index_list = [0]
joint_names = [str(joint_index) for joint_index in range(1,num_dof+1)]
robot_structure = [0,0,1,0]
output_feature = 'tau'


#### LOAD THE DATASET
print('---Load data and get the dataset')
# pos vel acc features
q_names = ['q_'+joint_name for joint_name in joint_names]
dq_names = ['dq_'+joint_name for joint_name in joint_names]
ddq_names = ['ddq_'+joint_name for joint_name in joint_names]
RBF_features = q_names + dq_names + ddq_names
# PP features
num_phi_nominal = 40
input_features_joint_list_PP = [['phi_'+str(joint_index+1)+'_'+str(phi_index+1) for phi_index in range(0,num_phi_nominal)]
                                 for joint_index in range(0,num_dof)]
# add the RBF features and the PP features
input_features=[]
for input_features_joint_PP in input_features_joint_list_PP:
    input_features += input_features_joint_PP
input_features = input_features+RBF_features
input_features_joint_list = [input_features_joint_list_PP[joint_index]+RBF_features
                            for joint_index in range(0,num_dof)]
# training dataset
input_tr, output_tr, active_dims_list = Project_Utils.get_data_from_features(tr_path, input_features, input_features_joint_list, output_feature, num_dof)
# test dataset
input_test, output_test, active_dims_list = Project_Utils.get_data_from_features(test_path, input_features, input_features_joint_list, output_feature, num_dof)
# normaliziation
if flg_norm:
    output_tr, norm_coeff = Project_Utils.normalize_signals(output_tr)
    output_test, _ = Project_Utils.normalize_signals(output_test, norm_coeff)
else:
    norm_coeff=None
# select the subset of training data to use
input_tr = input_tr[:num_dat_tr]
output_tr = output_tr[:num_dat_tr]
print('input_tr.shape: ', input_tr.shape)
print('input_test.shape: ', input_test.shape)


#### GET THE MODEL
print('---Get the model')
active_dims_list_PP = [active_dims[0:num_phi_nominal]
                       for active_dims in active_dims_list]
active_dims_list_RBF = [active_dims[-num_dof*3:]
                        for active_dims in active_dims_list]
sigma_n_init_list = [ 0.1*np.array([1.]) for link in range(0, num_dof)]
#pos def Sigma
Sigma_function_list = [ cov_functions.diagonal_covariance for link in range(0,num_dof)]
flg_ARD = True
Sigma_f_additional_par_list = [ [len(active_dims_link), flg_ARD] for active_dims_link in active_dims_list_PP]
Sigma_pos_par_init_list = [np.ones(additional_par_link[0]) for additional_par_link in Sigma_f_additional_par_list]
#RBF parameters
lengthscales_init_list = [np.ones(active_dims.size) for active_dims in active_dims_list_RBF]
flg_train_lengthscales_list = [True]*num_dof
lambda_init_list = [np.ones(1)]*num_dof
flg_train_lambda_list = [True]*num_dof 
# downsampling mode
downsampling_mode='Downsampling'
# set the device
if flg_cuda:
    device=torch.device('cuda:0')
else:
    device=torch.device('cpu')
# init the model
m = Models.m_ind_SP(num_dof, active_dims_list_PP, active_dims_list_RBF,
                    sigma_n_init_list=sigma_n_init_list, flg_train_sigma_n=True,
                    Sigma_function_list=Sigma_function_list, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                    Sigma_pos_par_init_list=Sigma_pos_par_init_list, flg_train_Sigma_pos_par=True,
                    Sigma_free_par_init_list=None, flg_train_Sigma_free_par=False,
                    lengthscales_init_list=lengthscales_init_list, flg_train_lengthscales_list=flg_train_lengthscales_list,
                    lambda_init_list=lambda_init_list, flg_train_lambda_list=flg_train_lambda_list,
                    name='SP_', dtype=torch.float64, max_input_loc=10000, downsampling_mode='Downsampling', sigma_n_num=None, device=device)
# move the model to the device
m.to(device)


#### TRAIN/LOAD THE MODEL
print('---Train/load the model')
torch.set_num_threads(1)
# load the model
if flg_load:
    print('Load the model...')
    m.load_state_dict(torch.load(model_loading_path))
# train the joint minimizing the negative MLL
if flg_train:
    print('Train the model with minimizing the negative MLL...')
    m.train_model(joint_index_list=joint_index_list,
                  X=input_tr, Y=output_tr,
                  criterion = Likelihood.Marginal_log_likelihood(), f_optimizer=f_optimizer,
                  batch_size=batch_size, shuffle = shuffle,
                  N_epoch=N_epoch, N_epoch_print=N_epoch_print)
# save the model
print('Save the model...')
if flg_save:
    torch.save(m.state_dict(), model_saving_path)


#### GET THE ESTIMATE
print('---Get estimate')
with torch.no_grad():
    print('Training estimate...')
    t_start = time.time()
    Y_tr_hat_list, var_tr_list, alpha_tr_list, m_X_list, K_X_inv_list, indices_list = m.get_torque_estimates(input_tr, output_tr, input_tr,
                                                                                                             joint_indices_list=joint_index_list,
                                                                                                             flg_return_K_X_inv=True)
    t_stop = time.time()
    print('Time elapsed ', t_stop-t_start)
    print('Test estimate...')
    t_start = time.time()
    Y_test_hat_list, var_test_list, _, _, _, _ = m.get_torque_estimates(input_tr, output_tr, input_test,
                                                                        alpha_list_par=alpha_tr_list, m_X_list_par=m_X_list,
                                                                        indices_list=indices_list,
                                                                        joint_indices_list=joint_index_list,
                                                                        K_X_inv_list_par=K_X_inv_list)
    t_stop = time.time()
    print('Time elapsed ', t_stop-t_start)


#### PRINT ESTIMATE AND STATS
Y_tr_hat_pd, Y_test_hat_pd, Y_tr_pd, Y_test_pd =  Project_Utils.get_pandas_obj(output_tr=output_tr,
                                                                               output_test=output_test,
                                                                               Y_tr_hat_list=Y_tr_hat_list,
                                                                               Y_test_hat_list=Y_test_hat_list,
                                                                               flg_norm=flg_norm,
                                                                               norm_coeff=norm_coeff,
                                                                               joint_index_list=joint_index_list,
                                                                               output_feature=output_feature,
                                                                               var_tr_list=var_tr_list,
                                                                               var_test_list=var_test_list)
# save estimates
if flg_save:
    pkl.dump(Y_tr_hat_pd, open(estimate_tr_saving_path, 'wb'))
    pkl.dump(Y_test_hat_pd, open(estimate_test_saving_path, 'wb'))
# # print the estimates
# Project_Utils.print_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# Project_Utils.print_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, flg_print_var=True, output_feature=output_feature)
# plt.show()
# get the erros stats
Project_Utils.get_stat_estimate(Y_tr_pd, [Y_tr_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)
Project_Utils.get_stat_estimate(Y_test_pd, [Y_test_hat_pd], joint_index_list, stat_name='nMSE', output_feature=output_feature)