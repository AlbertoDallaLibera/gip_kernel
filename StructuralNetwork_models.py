"""Utils functions for the method proposed in Structural Network modeling and control of rigid body robots
Author: Alberto Dalla Libera"""


import numpy as np
import torch
import itertools
import time
from scipy.signal import savgol_filter


def get_2dof_data(m1, m2, m3, m4, m5, X):
    """Return data from the 2dof example proposed in 'Structural Network modeling and control of rigid body robots'"""
    num_instants = X.shape[0]
    q = X[:,0:2]
    q_dot = X[:,2:4]
    q_ddot = X[:,4:6]
    tau = np.zeros((num_instants,2))
    for t in range(0,num_instants):
        M = np.array([[m1+m2+2*m3*np.cos(q[t,1]), m2+m3*np.cos(q[t,1])],
                      [m2+m3*np.cos(q[t,1]), m2]])
        C = np.array([[m3*q_dot[t,1], m3*(q_dot[t,0]+q_dot[t,1])*np.sin(q[t,1])],
                      [m3*q_dot[t,0]*np.sin(q[t,1]), 0.]])
        g = np.array([[m4*np.cos(q[t,0])+ m5*np.cos(q[t,0]+q[t,1])],
                      [m5*np.cos(q[t,0]+q[t,1])]])
        tau[t,:] = (np.matmul(M,q_ddot[t:t+1,:].transpose()) + np.matmul(C,q_dot[t:t+1,:].transpose()) + g).reshape([2])
    return tau


def get_2dof_trj(time_interval, sampling_time, f_q1, f_q2):
    """Returns the trjectory of the 2dof example"""
    t = np.arange(0,time_interval+sampling_time, sampling_time)
    q = np.concatenate([f_q1(t).reshape([-1,1]), 
                        f_q2(t).reshape([-1,1])], 1)
    q_dot = np.concatenate([savgol_filter(q[:,0], window_length=5, polyorder=3, deriv=1, delta=sampling_time, axis=-1, mode='interp', cval=0.0).reshape([-1,1]),
                            savgol_filter(q[:,1], window_length=5, polyorder=3, deriv=1, delta=sampling_time, axis=-1, mode='interp', cval=0.0).reshape([-1,1])],1)
    q_ddot = np.concatenate([savgol_filter(q[:,0], window_length=5, polyorder=3, deriv=2, delta=sampling_time, axis=-1, mode='interp', cval=0.0).reshape([-1,1]),
                             savgol_filter(q[:,1], window_length=5, polyorder=3, deriv=2, delta=sampling_time, axis=-1, mode='interp', cval=0.0).reshape([-1,1])],1)
    return np.concatenate([q, q_dot, q_ddot],1)


def get_monomials(X, deg_list, coef):
    """Torch function that compute all the possible monomials in x,
       with maximum deg of each elemetns of x equals to max_deg
       It requires:
       - X=[x1;...x_num_btc] = batch of data
       - deg_list = list with the degs of each monomial
       - coef vector: scaling coefficient of each monomial (shape [num_monomials,1])
    """
    batch_size, num_var = X.shape
    num_monomials = coef.shape[0]
    dtype = X.dtype
    device = X.device
    # initialize monomials at 1
    mon = torch.ones((batch_size,num_monomials), dtype=dtype, device=device)
    for mon_index in range(0,num_monomials):
        mon[:,mon_index] = torch.prod(torch.cat([X[:,var_index:var_index+1]**deg_list[mon_index,var_index]
                                                 for var_index in range(0,num_var)],1) ,1).squeeze()
    return mon*(coef.transpose(0,1))


def get_structural_info(num_rev, num_prism):
    """Computes all the informations required to compute the 
       regressors of the scara robot
    """
    num_joint = num_rev+num_prism
    num_base_var = 2*num_rev+num_prism #cos(q_rev)+sin(q_rev)+q_prism
    deg_indices = np.array([0,1,2])
    
    #compute the deg of all the possible monomials
    deg_n_list = []
    deg_2n_list = [] 
    for mon in itertools.product(deg_indices, repeat=num_base_var):
        if sum(mon) <= num_joint:
            deg_n_list.append(mon)
            deg_2n_list.append(mon)
        elif sum(mon)<= 2*num_joint:
            deg_2n_list.append(mon)
    deg_n = np.array(deg_n_list)
    deg_2n = np.array(deg_2n_list)
    coef_deg_n = np.ones((deg_n.shape[0],1))
    coef_deg_2n = np.ones((deg_2n.shape[0],1))
    print('number of monomials with deg <=4: ',deg_n.shape[0])
    print('number of monomials with deg <=8: ',deg_2n.shape[0])

    #get derivatives of poly with deg n
    #initilize degs
    deg_n_der_cos = []
    deg_n_der_sin = []
    deg_n_der_prism = []
    #initilize coefficients
    coef_deg_n_der_cos = []
    coef_deg_n_der_sin = []
    coef_deg_n_der_prism = []
    # get cos and sin derivative info
    for joint_index in range(0,num_rev):
        #cos derivatives
        deg_n_der_cos.append(np.copy(deg_n))
        coef_deg_n_der_cos.append(-np.copy(deg_n_der_cos[joint_index][:,joint_index]))
        # coef_deg_n_der_cos[joint_index][coef_deg_n_der_cos[joint_index]==0] = 0.
        non_zero_indices = deg_n_der_cos[joint_index][:,joint_index]
        deg_n_der_cos[joint_index][non_zero_indices,joint_index] -= 1
        deg_n_der_cos[joint_index][:,joint_index+num_rev] += 1
        #sin derivatives
        deg_n_der_sin.append(np.copy(deg_n))
        coef_deg_n_der_sin.append(np.copy(deg_n_der_sin[joint_index][:,joint_index+num_rev]))
        # coef_deg_n_der_sin[joint_index][coef_deg_n_der_sin[joint_index]==0] = 0.
        non_zero_indices = deg_n_der_sin[joint_index][:,joint_index+num_rev]!=0
        deg_n_der_sin[joint_index][non_zero_indices,joint_index+num_rev] -= 1
        deg_n_der_sin[joint_index][:,joint_index] += 1
    # get deg of prism derivative info
    for joint_index in range(2*num_rev, num_base_var):
        deg_n_der_prism.append(np.copy(deg_n))
        coef_deg_n_der_prism.append(np.copy(deg_n_der_prism[0][:,joint_index]))
        # coef_deg_n_der_prism[joint_index-2*num_rev][coef_deg_n_der_prism[joint_index-2*num_rev]==0] = 0.
        non_zero_indices = deg_n_der_prism[joint_index-2*num_rev][:,joint_index]!=0
        deg_n_der_prism[joint_index-2*num_rev][non_zero_indices,joint_index] -= 1

    #get derivatives of poly with deg 2n
    #initilize degs
    deg_2n_der_cos = []
    deg_2n_der_sin = []
    deg_2n_der_prism = []
    #initilize coefficients
    coef_deg_2n_der_cos = []
    coef_deg_2n_der_sin = []
    coef_deg_2n_der_prism = []
    # get cos and sin derivative info
    for joint_index in range(0,num_rev):
        #cos derivatives
        deg_2n_der_cos.append(np.copy(deg_2n))
        coef_deg_2n_der_cos.append(-np.copy(deg_2n_der_cos[joint_index][:,joint_index:joint_index+1]))
        # coef_deg_2n_der_cos[joint_index][coef_deg_2n_der_cos[joint_index]==0] = 0.
        non_zero_indices = deg_2n_der_cos[joint_index][:,joint_index]!=0
        deg_2n_der_cos[joint_index][non_zero_indices,joint_index] -= 1
        deg_2n_der_cos[joint_index][:,joint_index+num_rev] += 1
        #sin derivatives
        deg_2n_der_sin.append(np.copy(deg_2n))
        coef_deg_2n_der_sin.append(np.copy(deg_2n_der_sin[joint_index][:,joint_index+num_rev:joint_index+num_rev+1]))
        # coef_deg_2n_der_sin[joint_index][coef_deg_2n_der_sin[joint_index]==0] = 0.
        non_zero_indices = deg_2n_der_sin[joint_index][:,joint_index+num_rev]!=0
        deg_2n_der_sin[joint_index][non_zero_indices,joint_index+num_rev] -= 1
        deg_2n_der_sin[joint_index][:,joint_index] += 1
    # get deg of prism derivative info
    for joint_index in range(2*num_rev, num_base_var):
        deg_2n_der_prism.append(np.copy(deg_2n))
        coef_deg_2n_der_prism.append(np.copy(deg_2n_der_prism[joint_index-2*num_rev][:,joint_index:joint_index+1]))
        # coef_deg_2n_der_prism[joint_index-2*num_rev][coef_deg_2n_der_prism[joint_index-2*num_rev]==0] = 0.
        non_zero_indices = deg_2n_der_prism[joint_index-2*num_rev][:,joint_index]!=0
        deg_2n_der_prism[joint_index-2*num_rev][non_zero_indices,joint_index] -= 1

    deg_dict = {}
    deg_dict['deg_n'] = deg_n
    deg_dict['deg_n_der_cos'] = deg_n_der_cos
    deg_dict['deg_n_der_sin'] = deg_n_der_sin
    deg_dict['deg_n_der_prism'] = deg_n_der_prism
    deg_dict['deg_2n'] = deg_2n
    deg_dict['deg_2n_der_cos'] = deg_2n_der_cos
    deg_dict['deg_2n_der_sin'] = deg_2n_der_sin
    deg_dict['deg_2n_der_prism'] = deg_2n_der_prism
    deg_dict['coef_deg_n'] = coef_deg_n
    deg_dict['coef_deg_n_der_cos'] = coef_deg_n_der_cos
    deg_dict['coef_deg_n_der_sin'] = coef_deg_n_der_sin
    deg_dict['coef_deg_n_der_prism'] = coef_deg_n_der_prism
    deg_dict['coef_deg_2n'] = coef_deg_2n
    deg_dict['coef_deg_2n_der_cos'] = coef_deg_2n_der_cos
    deg_dict['coef_deg_2n_der_sin'] = coef_deg_2n_der_sin
    deg_dict['coef_deg_2n_der_prism'] = coef_deg_2n_der_prism
    return deg_dict


class Structural_network(torch.nn.Module):
    """Model of the structural_network of the scara robot"""
    def __init__(self, deg_dict,
                 num_rev, num_prism,
                 rev_indices, prism_indices,
                 num_monomials_2n, num_monomials_n,
                 dtype=torch.float64, device=torch.device('cpu')):
        super(Structural_network, self).__init__()
        self.num_rev = num_rev
        self.num_prism = num_prism
        self.num_joint = num_rev+num_prism
        self.rev_indices = rev_indices
        self.prism_indices = prism_indices
        self.num_monomials_n = num_monomials_n
        self.num_monomials_2n = num_monomials_2n
        self.dtype = dtype
        self.device = device
        self.deg_dict = deg_dict
        # initialize parameters vector
        self.num_par_vect_2n = int((self.num_joint**2-self.num_joint)/2 + self.num_joint)
        self.P = torch.nn.Parameter(torch.randn((num_monomials_2n, self.num_par_vect_2n), dtype=self.dtype, device=self.device), requires_grad=True)
        self.P_0 = torch.nn.Parameter(torch.randn((num_monomials_n,1), dtype=self.dtype, device=self.device), requires_grad=True)
        # move coefficients in torch
        self.coef_deg_2n = torch.tensor(deg_dict['coef_deg_2n'].reshape([-1,1]), dtype=dtype, device=device)
        if self.num_rev > 0:
            self.coef_deg_n_der_cos = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                       for coef in deg_dict['coef_deg_n_der_cos']]
            self.coef_deg_2n_der_cos = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                       for coef in deg_dict['coef_deg_2n_der_cos']]
            self.coef_deg_n_der_sin = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                       for coef in deg_dict['coef_deg_n_der_sin']]
            self.coef_deg_2n_der_sin = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                        for coef in deg_dict['coef_deg_2n_der_sin']]
        if self.num_prism:
            self.coef_deg_n_der_prism = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                         for coef in deg_dict['coef_deg_n_der_prism']]
            self.coef_deg_2n_der_prism = [torch.tensor(coef, dtype=dtype, device=device).reshape([-1,1])
                                          for coef in deg_dict['coef_deg_2n_der_prism']]


    def map_data(self, X):
        """Method that maps inputs in the augmented space, and reorder joints signal
           such that the first num_rev joints are revolute joints
           X = [q, q_dot, q_ddot]
        """
        # get a list with position
        X_pos_list = []
        X_pos_list.append(torch.cos(X[:,0:self.num_joint][:,self.rev_indices]))
        X_pos_list.append(torch.sin(X[:,0:self.num_joint][:,self.rev_indices]))
        X_pos_list.append(X[:,0:self.num_joint][:,self.prism_indices])
        # get a list with velocities
        X_vel_list = []
        X_vel_list.append(X[:,self.num_joint:2*self.num_joint][:,self.rev_indices])
        X_vel_list.append(X[:,self.num_joint:2*self.num_joint][:,self.prism_indices])
        # get a list with accelerations
        X_acc_list = []
        X_acc_list.append(X[:,2*self.num_joint:][:,self.rev_indices])
        X_acc_list.append(X[:,2*self.num_joint:][:,self.prism_indices])
        return torch.cat(X_pos_list,1), torch.cat(X_vel_list,1), torch.cat(X_acc_list,1)


    def map_output(self, tau):
        """Reorder the output vector"""
        tau_out = []
        current_order =  self.rev_indices+self.prism_indices
        for joint_index in range(0,self.num_joint):
            index = current_order.index(joint_index)
            tau_out.append(tau[:,index:index+1])
        return torch.cat(tau_out,1)


    def get_D_monomials(self, X_pos):
        """Returns the monomials of matrix D"""
        return get_monomials(X_pos, self.deg_dict['deg_2n'], self.coef_deg_2n)


    def get_C_monomials(self, X_pos):
        """Returns the monomials of matrix C"""
        monomials_list = []
        # get derivatives respect to the revolute joints
        for joint in range(0,self.num_rev):
            monomials_list.append(get_monomials(X_pos, self.deg_dict['deg_2n_der_cos'][joint], self.coef_deg_2n_der_cos[joint]) +
                                  get_monomials(X_pos, self.deg_dict['deg_2n_der_sin'][joint], self.coef_deg_2n_der_sin[joint]))
        # get derivatives respect to the prismatic joints
        for joint in range(self.num_rev,self.num_joint):
            monomials_list.append(get_monomials(X_pos, self.deg_dict['deg_2n_der_prism'][joint-self.num_rev], self.coef_deg_2n_der_prism[joint-self.num_rev]))
        return monomials_list


    def get_g_monomials(self, X_pos):
        """Returns the monomials of matrix C"""
        monomials_list = []
        # get derivatives respect to the revolute joints
        for joint in range(0,self.num_rev):
            monomials_list.append(get_monomials(X_pos, self.deg_dict['deg_n_der_cos'][joint], self.coef_deg_n_der_cos[joint]) +
                                  get_monomials(X_pos, self.deg_dict['deg_n_der_sin'][joint], self.coef_deg_n_der_sin[joint]))
        # get derivatives respect to the prismatic joints
        for joint in range(self.num_rev,self.num_joint):
            monomials_list.append(get_monomials(X_pos, self.deg_dict['deg_n_der_prism'][joint-self.num_rev], self.coef_deg_n_der_prism[joint-self.num_rev]))
        # return torch.cat(monomials_list, 0)
        return monomials_list


    def get_P_index(self,i,j):
        """compute index of parameters associated to
           the element in position (i,j)
        """
        if j > i:
            tmp = i
            i = j
            j = tmp
        return int(j+(i**2-i)/2+i)


    def get_D(self, monomials_2n):
        """Returns the batch of D matrices"""
        # get the elements of D
        D_elements = torch.matmul(monomials_2n, self.P)
        # allocate the space
        batch_size = monomials_2n.shape[0]
        D = torch.zeros((batch_size, self.num_joint, self.num_joint),
                        dtype=self.dtype,
                        device=self.device)
        #assign the elements
        curretn_index = 0
        for i in range(0, self.num_joint):
            for j in range(0, i):
                D[:,i,j] = D_elements[:,curretn_index]
                D[:,j,i] = D_elements[:,curretn_index]
                curretn_index +=1
        return D


    def get_C(self,monomials_2n_der_list, X_vel):
        """Compute batch of C matrices"""
        # allocate the space
        batch_size = monomials_2n_der_list[0].shape[0]
        C = torch.zeros((batch_size, self.num_joint, self.num_joint),
                        dtype=self.dtype,
                        device=self.device)
        for k in range(0,self.num_joint):
            for j in range(0,self.num_joint):
                for i in range(0, self.num_joint):
                    C[:,k,j] += 0.5*(torch.matmul(monomials_2n_der_list[i],self.P[:,self.get_P_index(k,j)])+
                                     torch.matmul(monomials_2n_der_list[j],self.P[:,self.get_P_index(k,i)])-
                                     torch.matmul(monomials_2n_der_list[k],self.P[:,self.get_P_index(i,j)]))*X_vel[:,i]
        return C


    def get_g(self, monomials_g):
        """compute the g vector"""
        batch_size = int(monomials_g[0].shape[0])
        g_elements_list = [torch.matmul(monomials_g[joint_index], self.P_0) for joint_index in range(0,self.num_joint)] 
        return torch.cat(g_elements_list,1).unsqueeze(-1)


    def forward(self,X):
        """Compute the robot dynamics with structural network"""
        # get pos, vel and acc data
        X_pos, X_vel, X_acc = self.map_data(X)
        # get monomials
        monomials_D = self.get_D_monomials(X_pos)
        monomials_C = self.get_C_monomials(X_pos)
        monomials_g = self.get_g_monomials(X_pos)
        # get matrices
        D = self.get_D(monomials_D)
        C = self.get_C(monomials_C, X_vel)
        g = self.get_g(monomials_g)
        # get the batch of torques
        tau = (torch.matmul(D, X_acc.unsqueeze(-1)) + torch.matmul(C, X_vel.unsqueeze(-1)) + g ).squeeze()
        return self.map_output(tau.reshape([-1, self.num_joint]))


    def get_estimates(self, X):
        """Compute the estimates from numpy inputs"""
        tau_tc = self(torch.tensor(X, dtype=self.dtype, device=self.device))
        return tau_tc.detach().cpu().numpy()


    def train_model(self, X, Y,
                    criterion, optimizer,
                    batch_size, shuffle,
                    N_epoch, N_epoch_print):
        """Train the structural network"""
        # get the dataloader
        dataset = torch.utils.data.TensorDataset(torch.tensor(X,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device),
                                                 torch.tensor(Y,
                                                              requires_grad=False,
                                                              dtype=self.dtype,
                                                              device=self.device))
        trainloader = torch.utils.data.DataLoader(dataset,
                                                  batch_size=batch_size,
                                                  shuffle=shuffle)
        # trian the model
        for epoch in range(N_epoch):
            t_start = time.time()
            running_loss = 0.0
            N_btc = 0
            # print('\nEPOCH:', epoch)
            for i, data in enumerate(trainloader, 0):
                # get the inputs
                inputs, labels = data
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward + backward + optimize
                out = self(inputs)
                loss = criterion(out, labels)
                loss.backward(retain_graph=True)
                optimizer.step()
                running_loss = running_loss + loss.item()
                N_btc = N_btc + 1
            t_stop = time.time()
            print(t_stop-t_start)
            if epoch%N_epoch_print==0:
                print('\nEPOCH:', epoch)
                print('Runnung loss:', running_loss/N_btc)